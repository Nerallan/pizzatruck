package com.nerallan.core.db.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "Pizza")
data class Pizza constructor(
    @PrimaryKey
    @ColumnInfo(name = "name")
    @SerializedName("stringValue")
    var name: String,

    @ColumnInfo(name = "ingredients")
    @SerializedName("stringValue")
    var ingredients: String,

    @ColumnInfo(name = "cost")
    @SerializedName("stringValue")
    var cost: String,

    @ColumnInfo(name = "image")
    @SerializedName("stringValue")
    var image: String,

    @ColumnInfo(name = "details_uri")
    var detailsUri: String
) : Parcelable