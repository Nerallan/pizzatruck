package com.nerallan.core.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.nerallan.core.Config
import com.nerallan.core.db.entity.Pizza
import com.nerallan.core.db.dao.AdDao
import com.nerallan.core.db.dao.PizzaDao
import com.nerallan.core.db.entity.Ad

@Database(entities = [Pizza::class, Ad::class], version = 2)
abstract class PizzaDatabase : RoomDatabase() {
    abstract fun pizzaDao(): PizzaDao
    abstract fun adDao(): AdDao

    companion object {

        @Volatile
        private var INSTANCE: PizzaDatabase? = null
        fun getInstance(context: Context): PizzaDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it}
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context.applicationContext,
                PizzaDatabase::class.java, Config.DATABASE_NAME)
                .addMigrations(MIGRATION_1_2)
                .build()

        @JvmField
        val MIGRATION_1_2 = object: Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE Ad "+ " ADD COLUMN order_index INTEGER default 1 not null ")
            }
        }
    }
}