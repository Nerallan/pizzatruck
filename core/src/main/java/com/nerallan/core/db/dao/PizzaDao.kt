package com.nerallan.core.db.dao

import androidx.room.*
import com.nerallan.core.db.entity.Pizza
import io.reactivex.Single

@Dao
interface PizzaDao {
    @Query("SELECT * FROM Pizza")
    fun getAll(): Single<List<Pizza>>

    @Query("SELECT * FROM Pizza WHERE details_uri LIKE :detailsUri")
    fun getByUri(detailsUri: String): Single<Pizza>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(pizza: List<Pizza>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(pizza: Pizza)

    @Query("DELETE FROM Pizza")
    fun clearTable()
}