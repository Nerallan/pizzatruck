package com.nerallan.core.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Ad")
data class Ad (
    @PrimaryKey
    @ColumnInfo(name = "name")
    var imageUri: String,
    @ColumnInfo(name = "order_index")
    var orderIndex: Int
)