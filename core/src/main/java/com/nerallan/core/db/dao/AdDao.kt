package com.nerallan.core.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.nerallan.core.db.entity.Ad
import io.reactivex.Single

@Dao
interface AdDao {
    @Query("SELECT * FROM Ad")
    fun getAll(): Single<List<Ad>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(pizza: List<Ad>)
}