package com.nerallan.core.model

import com.google.gson.annotations.SerializedName

data class PizzaDocumentResponse(
    @SerializedName("name")
    var docName: String,
    @SerializedName("fields")
    var pizzaFieldsResponse: PizzaFieldsResponse,
    @SerializedName("createTime")
    var createTime: String,
    @SerializedName("updateTime")
    var updateTime: String
)