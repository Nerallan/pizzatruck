package com.nerallan.core.model

import com.google.gson.annotations.SerializedName

data class AdDocumentResponse (
    @SerializedName("name")
    var docName: String,
    @SerializedName("fields")
    var adFieldsResponse: AdFieldsResponse,
    @SerializedName("createTime")
    var createTime: String,
    @SerializedName("updateTime")
    var updateTime: String
)