package com.nerallan.core.model

import com.google.gson.JsonObject
import com.google.gson.annotations.SerializedName

data class PizzaFieldsResponse(
    @SerializedName("name")
    var nameObject: JsonObject,
    @SerializedName("ingredients")
    var ingredientsObject: JsonObject,
    @SerializedName("cost")
    var costObject: JsonObject,
    @SerializedName("image")
    var imageObject: JsonObject
)