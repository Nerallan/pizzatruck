package com.nerallan.core.model

import com.google.gson.JsonObject
import com.google.gson.annotations.SerializedName

data class AdFieldsResponse(
    @SerializedName("uri")
    var uri: JsonObject,
    @SerializedName("order")
    var order: JsonObject
)