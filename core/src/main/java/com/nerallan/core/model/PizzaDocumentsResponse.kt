package com.nerallan.core.model

import com.google.gson.annotations.SerializedName

data class PizzaDocumentsResponse (
    @SerializedName("documents")
    var pizzaDocumentResponses: List<PizzaDocumentResponse>
)