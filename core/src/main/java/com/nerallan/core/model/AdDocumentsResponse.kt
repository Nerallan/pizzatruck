package com.nerallan.core.model

import com.google.gson.annotations.SerializedName

data class AdDocumentsResponse (
    @SerializedName("documents")
    var adDocumentResponse: List<AdDocumentResponse>
)