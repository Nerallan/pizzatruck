package com.nerallan.core

class Config {
    companion object {
        const val BASE_API_URL = "https://firestore.googleapis.com/v1/projects/pizzatruck-ed1d4/databases/(default)/documents/"
        const val ROUTE_DIRECTION_URL = "https://www.mapquestapi.com/directions/v2/"
        const val PIZZA_IMAGES_STORAGE_REF = "pizza_images/"
        const val AD_IMAGES_STORAGE_REF = "ad_images/"
        const val URI_AD_IMAGES_SUBSTRING = "/ad_images/"
        const val URI_MENU_SUBSTRING = "/menu/"
        const val INTENT_IMAGE_TYPE = "image/*"
        const val DATABASE_NAME = "pizza_database"
        val IMAGE_MIME_TYPE = arrayOf("image/jpg", "image/png")
    }
}