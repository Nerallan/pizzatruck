package com.nerallan.core.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nerallan.core.R
import com.nerallan.core.db.entity.Pizza

class PizzaListAdapter (
    private val adapterPizzaList: ArrayList<Pizza>,
    private val clickListener: (Int) -> Unit)
    : RecyclerView.Adapter<PizzaViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PizzaViewHolder {
        return PizzaViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.list_row,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: PizzaViewHolder, position: Int) {
        holder.bind(adapterPizzaList[position], clickListener)
    }

    override fun getItemCount(): Int {
        return adapterPizzaList.size
    }

    fun removeItem(position: Int) {
        adapterPizzaList.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, adapterPizzaList.size)
    }

    fun restoreItem(pizza: Pizza, position: Int) {
        adapterPizzaList.add(position, pizza)
        notifyItemInserted(position)
    }
}