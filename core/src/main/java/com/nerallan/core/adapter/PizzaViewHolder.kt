package com.nerallan.core.adapter

import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.nerallan.core.R
import com.nerallan.core.db.entity.Pizza
import kotlinx.android.synthetic.main.list_row.view.*

class PizzaViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(pizza: Pizza, clickListener: (Int) -> Unit) = with(itemView) {
        pizza_name_text_view.text = pizza.name
        pizza_ingredients_text_view.text = pizza.ingredients
        cost_text_view.text = pizza.cost

        val requestOptions = RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL)
        Glide
            .with(this)
            .load(pizza.image)
            .placeholder(ContextCompat.getDrawable(itemView.context, R.drawable.ic_pizza_placeholder))
            .apply(requestOptions)
            .into(pizza_image_view)

        itemView.setOnClickListener {
            clickListener(adapterPosition)
        }
    }
}