package com.nerallan.admin

sealed class ItemType(val typeId: Int) {
    object Item : ItemType(0)
    object PlusItem : ItemType(1)
}
