package com.nerallan.admin

sealed class ResponseStatus {
    object Loading : ResponseStatus()
    object Success : ResponseStatus()
    data class Error(val errorMessage: String) : ResponseStatus()
}