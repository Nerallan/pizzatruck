package com.nerallan.admin.datasource

import androidx.lifecycle.MutableLiveData
import com.nerallan.admin.ResponseStatus
import com.nerallan.admin.model.Coordinates

interface MapDataSource {
    fun addCoordinates(coordinates: Coordinates, status: MutableLiveData<ResponseStatus>): MutableLiveData<ResponseStatus>
}