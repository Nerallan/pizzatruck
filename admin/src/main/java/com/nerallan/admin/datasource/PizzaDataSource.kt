package com.nerallan.admin.datasource

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.nerallan.admin.ResponseStatus
import com.nerallan.admin.model.PizzaResponse
import com.nerallan.core.db.entity.Pizza

interface PizzaDataSource {

    fun putImageToDataStore(selectedImageUri: Uri, callback: ImageLoader.LoadImageCallback)

    fun getImageUri(selectedImageUri: Uri, callback: ImageLoader.GetImageUriCallback)

    fun getPizzaList(data: MutableLiveData<List<Pizza>>): MutableLiveData<List<Pizza>>

    fun addPizza(pizza: PizzaResponse, status: MutableLiveData<ResponseStatus>): LiveData<ResponseStatus>

    fun deletePizza(
        documentId: String,
        deleteItemStatus: MutableLiveData<ResponseStatus>
    ): MutableLiveData<ResponseStatus>
}