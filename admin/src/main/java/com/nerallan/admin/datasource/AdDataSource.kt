package com.nerallan.admin.datasource

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.nerallan.admin.ResponseStatus
import com.nerallan.admin.model.AdDTO

interface AdDataSource {
    fun putImageToDataStore(selectedImageUri: Uri, callback: ImageLoader.LoadImageCallback)

    fun getImageUri(selectedImageUri: Uri, callback: ImageLoader.GetImageUriCallback)

    fun pushAd(
        adRequest: AdDTO,
        status: MutableLiveData<ResponseStatus>,
        callback: () -> Unit
    ): LiveData<ResponseStatus>

    fun updateAdList(
        adList: MutableList<AdDTO>,
        responseStatus: MutableLiveData<ResponseStatus>
    ): MutableLiveData<ResponseStatus>

    fun getAdList(
        data: MutableLiveData<MutableList<AdDTO>>,
        responseStatus: MutableLiveData<ResponseStatus>
    ): MutableLiveData<MutableList<AdDTO>>

    fun deleteAd(
        documentId: String,
        status: MutableLiveData<ResponseStatus>,
        callback: () -> Unit
    ): MutableLiveData<ResponseStatus>
}