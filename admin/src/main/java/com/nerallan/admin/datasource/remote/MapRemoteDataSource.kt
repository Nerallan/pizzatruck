package com.nerallan.admin.datasource.remote

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.nerallan.admin.ResponseStatus
import com.nerallan.admin.api.MapApiInterface
import com.nerallan.admin.datasource.MapDataSource
import com.nerallan.admin.model.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber

class MapRemoteDataSource(private val mapApi: MapApiInterface) : MapDataSource {

    companion object {
        private const val TAG: String = "MapRemoteDataSource"
    }

    override fun addCoordinates(
        coordinates: Coordinates,
        status: MutableLiveData<ResponseStatus>
    ): MutableLiveData<ResponseStatus> {
        val result: Call<JsonObject> = mapApi.pushCoordinatesToServer(makeJsonFromLatLng(coordinates))
        result.enqueue(object : Callback<JsonObject> {
            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                status.value = ResponseStatus.Error(t.message.toString())
                Timber.d( "onFailure ${t.message}")
            }

            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                if (response.isSuccessful) {
                    status.value = ResponseStatus.Success
                    Timber.d("onResponse ${response.body()}")
                } else {
                    status.value = ResponseStatus.Error(response.message())
                    Timber.d("onResponse ${response.body()}")
                }
            }
        })
        return status
    }

    private fun makeJsonFromLatLng(coordinates: Coordinates): JsonObject {
        val locationLat: LatRequest = LatRequest(coordinates.lat.toString())
        val locationLng: LngRequest = LngRequest(coordinates.lng.toString())
        val locationFieldsRequest: LocationFieldsRequest = LocationFieldsRequest(locationLat, locationLng)
        val locationDocumentRequest: LocationDocumentRequest = LocationDocumentRequest(locationFieldsRequest)

        val gson: Gson = Gson()
        val json: String = gson.toJson(locationDocumentRequest)
        val jsonObject: JsonObject = JsonParser().parse(json).asJsonObject
        return jsonObject
    }
}