package com.nerallan.admin.datasource

import android.net.Uri

interface ImageLoader {

    interface LoadImageCallback {

        fun onImageLoaded(result: String)

        fun onErrorOccurred(error: String)
    }

    interface GetImageUriCallback {

        fun onUriReceived(uri: Uri)

        fun onErrorOccurred(error: String)
    }
}