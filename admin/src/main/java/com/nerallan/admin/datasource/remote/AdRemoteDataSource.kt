package com.nerallan.admin.datasource.remote

import android.net.Uri
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.nerallan.admin.ResponseStatus
import com.nerallan.admin.api.AdApiInterface
import com.nerallan.admin.datasource.AdDataSource
import com.nerallan.admin.datasource.ImageLoader
import com.nerallan.admin.model.*
import com.nerallan.admin.ui.AdServerOrderComparator
import com.nerallan.core.Config
import com.nerallan.core.model.AdDocumentResponse
import com.nerallan.core.model.AdFieldsResponse
import com.nerallan.core.model.AdDocumentsResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber

class AdRemoteDataSource(private val adApi: AdApiInterface) : AdDataSource {

    private var storageReference: StorageReference = FirebaseStorage.getInstance().reference

    companion object {
        private const val TAG: String = "AdRemoteDataSource"
        private const val STRING_VALUE_KEY = "stringValue"
    }

    override fun putImageToDataStore(selectedImageUri: Uri, callback: ImageLoader.LoadImageCallback) {
        val adRef = storageReference.child(Config.AD_IMAGES_STORAGE_REF + selectedImageUri.lastPathSegment)
        val uploadTask = adRef.putFile(selectedImageUri)

        uploadTask.addOnSuccessListener { taskSnapshot: UploadTask.TaskSnapshot ->
            if (taskSnapshot.task.isSuccessful) {
                Timber.d("putImageToDataStore ${taskSnapshot.task.result}")
                callback.onImageLoaded(taskSnapshot.task.result.toString())
            }
        }.addOnFailureListener { exception: Exception ->
            Timber.d( "putImageToDataStore ${exception.message}")
            callback.onErrorOccurred(exception.message.toString())
        }
    }

    override fun getImageUri(selectedImageUri: Uri, callback: ImageLoader.GetImageUriCallback) {
        val adRef = storageReference.child(Config.AD_IMAGES_STORAGE_REF + selectedImageUri.lastPathSegment)
        val uploadTask = adRef.putFile(selectedImageUri)

        uploadTask.continueWithTask { task ->
            return@continueWithTask adRef.downloadUrl
        }.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val downloadUri = task.result
                downloadUri?.let { callback.onUriReceived(it) }
            } else {
                callback.onErrorOccurred(task.exception?.message.toString())
            }
        }
    }

    override fun updateAdList(
        adList: MutableList<AdDTO>,
        responseStatus: MutableLiveData<ResponseStatus>
    ): MutableLiveData<ResponseStatus> {
        // TODO update orders in list of changed items
        for (ad in adList) {
            val result: Call<JsonObject> = adApi.updateAd(ad.imageId, makeJsonFieldsFromAd(ad))
            result.enqueue(object : Callback<JsonObject> {
                override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                    responseStatus.value = ResponseStatus.Error(t.message.toString())
                    Timber.d("onFailure ${t.message}")
                }

                override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                    responseStatus.value = ResponseStatus.Success
                    Timber.d( "onResponse ${response.body()}")
                }
            })
        }
        return responseStatus
    }

    override fun pushAd(
        adRequest: AdDTO,
        status: MutableLiveData<ResponseStatus>,
        callback: () -> Unit
    ): LiveData<ResponseStatus> {
        val result: Call<JsonObject> = adApi.pushAdToServer(makeJsonFieldsFromAd(adRequest))
        result.enqueue(object : Callback<JsonObject> {
            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                status.value = ResponseStatus.Error(t.message.toString())
                Timber.d("onFailure ${t.message}")
            }

            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                status.value = ResponseStatus.Success
                Timber.d( "onResponse ${response.body()}")
                callback()
            }
        })
        return status
    }

    override fun getAdList(
        data: MutableLiveData<MutableList<AdDTO>>,
        responseStatus: MutableLiveData<ResponseStatus>
    ): MutableLiveData<MutableList<AdDTO>> {
        val call: Call<AdDocumentsResponse> = adApi.getAdList()
        Timber.d("call $call")
        call.enqueue(object : Callback<AdDocumentsResponse> {
            override fun onFailure(call: Call<AdDocumentsResponse>, t: Throwable) {
                Timber.d( "failure ${t.message}")
                responseStatus.value = ResponseStatus.Error(t.message.toString())
            }

            override fun onResponse(call: Call<AdDocumentsResponse>, responseBody: Response<AdDocumentsResponse>) {
                val menuResponseBody: AdDocumentsResponse? = responseBody.body()
                val menuDocumentResponses: List<AdDocumentResponse>? = menuResponseBody?.adDocumentResponse
                val adUriList = ArrayList<AdDTO>()
                for (doc in menuDocumentResponses!!) {
                    val adFieldsResponse: AdFieldsResponse = doc.adFieldsResponse
                    val adId: String = doc.docName.substringAfterLast(Config.URI_AD_IMAGES_SUBSTRING)
                    val uri: String = adFieldsResponse.uri.get(STRING_VALUE_KEY).asString
                    val order: Int = adFieldsResponse.order.get(STRING_VALUE_KEY).asInt
                    adUriList.add(AdDTO(uri, adId, order))
                    responseStatus.value = ResponseStatus.Success
                }
                // sorting server data by order field
                val sortedList = ArrayList<AdDTO>()
                sortedList.addAll(sortAdList(adUriList))
                data.value = sortedList
            }
        })
        return data
    }

    override fun deleteAd(
        documentId: String,
        status: MutableLiveData<ResponseStatus>,
        callback: () -> Unit
    ): MutableLiveData<ResponseStatus> {
        val result: Call<JsonObject> = adApi.deleteAdFromServer(documentId)
        result.enqueue(object : Callback<JsonObject> {
            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                Timber.d("onFailure ${t.message}")
                status.value = ResponseStatus.Error(t.message.toString())
            }

            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                Timber.d( "onResponse ${response.body()}")
                status.value = ResponseStatus.Success
                callback()
            }
        })
        return status
    }

    private fun makeJsonFieldsFromAd(adDTO: AdDTO): JsonObject {
        val adUri: AdUri = AdUri(adDTO.imageUri)
        val adOrder: AdOrder = AdOrder(adDTO.order.toString())
        val adFieldsRequest: AdFieldsRequest = AdFieldsRequest(adUri, adOrder)
        val adDocument: AdDocumentRequest = AdDocumentRequest(adFieldsRequest)

        val gson: Gson = Gson()
        val json: String = gson.toJson(adDocument)
        val jsonObject: JsonObject = JsonParser().parse(json).asJsonObject
        return jsonObject
    }

    private fun sortAdList(list: List<AdDTO>): List<AdDTO> {
        return list.sortedWith(AdServerOrderComparator)
    }
}