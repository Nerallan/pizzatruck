package com.nerallan.admin.datasource.remote

import android.net.Uri
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import com.google.gson.JsonObject
import com.google.gson.*
import com.nerallan.admin.ResponseStatus
import com.nerallan.admin.api.PizzaApiInterface
import com.nerallan.admin.datasource.ImageLoader
import com.nerallan.admin.datasource.PizzaDataSource
import com.nerallan.admin.model.*
import com.nerallan.core.Config
import com.nerallan.core.db.entity.Pizza
import com.nerallan.core.model.PizzaDocumentResponse
import com.nerallan.core.model.PizzaFieldsResponse
import com.nerallan.core.model.PizzaDocumentsResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber

class PizzaRemoteDataSource(private val pizzaApi: PizzaApiInterface) : PizzaDataSource {

    private var storageReference: StorageReference = FirebaseStorage.getInstance().reference

    companion object {
        private const val TAG: String = "PizzaRemoteDataSource"
        private const val STRING_VALUE_KEY = "stringValue"
    }

    override fun getPizzaList(data: MutableLiveData<List<Pizza>>): MutableLiveData<List<Pizza>> {
        val call: Call<PizzaDocumentsResponse> = pizzaApi.getMenus()
        Timber.d("call $call")
        call.enqueue(object : Callback<PizzaDocumentsResponse> {
            override fun onFailure(call: Call<PizzaDocumentsResponse>, t: Throwable) {
                Timber.d("failure ${t.message}")
            }

            override fun onResponse(call: Call<PizzaDocumentsResponse>, response: Response<PizzaDocumentsResponse>) {
                val pizzaDocumentsResponse: PizzaDocumentsResponse? = response.body()
                val pizzaDocumentResponses: List<PizzaDocumentResponse>? = pizzaDocumentsResponse?.pizzaDocumentResponses
                val pizzaList = ArrayList<Pizza>()
                for (doc in pizzaDocumentResponses!!) {
                    val pizzaFieldsResponse: PizzaFieldsResponse = doc.pizzaFieldsResponse
                    val detailsId: String = doc.docName.substringAfterLast(Config.URI_MENU_SUBSTRING)
                    val gson: Gson = Gson()
                    pizzaList.add(
                        Pizza(
                            pizzaFieldsResponse.nameObject.get(STRING_VALUE_KEY).asString,
                            pizzaFieldsResponse.ingredientsObject.get(STRING_VALUE_KEY).asString,
                            pizzaFieldsResponse.costObject.get(STRING_VALUE_KEY).asString,
                            pizzaFieldsResponse.imageObject.get(STRING_VALUE_KEY).asString,
                            detailsId
                        )
                    )
                    data.value = pizzaList
                }
            }
        })
        return data
    }

    override fun putImageToDataStore(selectedImageUri: Uri, callback: ImageLoader.LoadImageCallback) {
        val pizzaRef = storageReference.child(Config.PIZZA_IMAGES_STORAGE_REF + selectedImageUri.lastPathSegment)
        val uploadTask = pizzaRef.putFile(selectedImageUri)

        uploadTask.addOnSuccessListener { taskSnapshot: UploadTask.TaskSnapshot ->
            if (taskSnapshot.task.isSuccessful) {
                Timber.d( "putImageToDataStore ${taskSnapshot.task.result}")
                callback.onImageLoaded(taskSnapshot.task.result.toString())
            }
        }.addOnFailureListener { exception: Exception ->
            Timber.d( "putImageToDataStore ${exception.message}")
            callback.onErrorOccurred(exception.message.toString())
        }
    }

    override fun getImageUri(selectedImageUri: Uri, callback: ImageLoader.GetImageUriCallback) {
        val pizzaRef = storageReference.child(Config.PIZZA_IMAGES_STORAGE_REF + selectedImageUri.lastPathSegment)
        val uploadTask = pizzaRef.putFile(selectedImageUri)

        val urlTask = uploadTask.continueWithTask { task ->
            if (!task.isSuccessful) {
                task.exception?.let {
                    throw it
                }
            }
            pizzaRef.downloadUrl
        }.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val downloadUri = task.result
                downloadUri?.let { callback.onUriReceived(it) }
            } else {
                callback.onErrorOccurred(task.exception?.message.toString())
            }
        }
    }

    override fun addPizza(
        pizza: PizzaResponse,
        status: MutableLiveData<ResponseStatus>
    ): MutableLiveData<ResponseStatus> {
        val result: Call<JsonObject> = pizzaApi.pushPizzaToServer(makeJsonFromPizza(pizza))
        result.enqueue(object : Callback<JsonObject> {
            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                status.value = ResponseStatus.Error(t.message.toString())
                Timber.d( "onFailure ${t.message}")
            }

            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                status.value = ResponseStatus.Success
                Timber.d("onResponse ${response.body()}")
            }
        })
        return status
    }

    override fun deletePizza(
        documentId: String,
        deleteItemStatus: MutableLiveData<ResponseStatus>
    ): MutableLiveData<ResponseStatus> {
        val result: Call<JsonObject> = pizzaApi.deletePizzaFromServer(documentId)
        result.enqueue(object : Callback<JsonObject> {
            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                Timber.d( "onFailure ${t.message}")
                deleteItemStatus.value = ResponseStatus.Error(t.message.toString())
            }

            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                Timber.d( "onResponse ${response.body()}")
                deleteItemStatus.value = ResponseStatus.Success
            }
        })
        return deleteItemStatus
    }

    private fun makeJsonFromPizza(pizza: PizzaResponse): JsonObject {
        val pizzaNameValue: PizzaName = PizzaName(pizza.name)
        val pizzaImageValue: PizzaImage = PizzaImage(pizza.image)
        val pizzaIngredientsValue: PizzaIngredients = PizzaIngredients(pizza.ingredients)
        val pizzaCostValue: PizzaCost = PizzaCost(pizza.cost)
        val pizzaFieldsRequest: PizzaFieldsRequest = PizzaFieldsRequest(pizzaNameValue, pizzaIngredientsValue, pizzaCostValue, pizzaImageValue)
        val pizzaDocumentRequest: PizzaDocumentRequest = PizzaDocumentRequest(pizzaFieldsRequest)

        val gson: Gson = Gson()
        val json: String = gson.toJson(pizzaDocumentRequest)
        val jsonObject: JsonObject = JsonParser().parse(json).asJsonObject
        return jsonObject
    }
}