package com.nerallan.admin.api

import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.PATCH

interface MapApiInterface {
    @PATCH("location/coordinates")
    fun pushCoordinatesToServer(@Body documentObject: JsonObject): Call<JsonObject>
}