package com.nerallan.admin.api

import com.nerallan.core.Config
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object PizzaApiClient {

    val getClient: PizzaApiInterface by lazy {
        val retrofit = Retrofit.Builder()
            .baseUrl(Config.BASE_API_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        retrofit.create(PizzaApiInterface::class.java)
    }
}