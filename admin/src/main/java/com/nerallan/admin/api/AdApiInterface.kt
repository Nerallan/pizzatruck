package com.nerallan.admin.api

import com.google.gson.JsonObject
import com.nerallan.core.model.AdDocumentsResponse
import retrofit2.Call
import retrofit2.http.*

interface AdApiInterface {
    @GET("ad_images")
    fun getAdList(): Call<AdDocumentsResponse>

    @POST("ad_images")
    fun pushAdToServer(@Body documentObject: JsonObject): Call<JsonObject>

    @PATCH("ad_images/{id}?currentDocument.exists=true&updateMask.fieldPaths=order")
    fun updateAd(@Path("id") documentId: String, @Body documentObject: JsonObject): Call<JsonObject>

    @DELETE("ad_images/{id}")
    fun deleteAdFromServer(@Path("id") documentId: String): Call<JsonObject>
}