package com.nerallan.admin.api

import com.google.gson.JsonObject
import com.nerallan.core.model.PizzaDocumentsResponse
import retrofit2.Call
import retrofit2.http.*

interface PizzaApiInterface {
    @GET("menu")
    fun getMenus(): Call<PizzaDocumentsResponse>

    @POST("menu")
    fun pushPizzaToServer(@Body pizzaJson: JsonObject): Call<JsonObject>

    @DELETE("menu/{id}")
    fun deletePizzaFromServer(@Path("id") documentId: String): Call<JsonObject>
}