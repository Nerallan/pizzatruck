package com.nerallan.admin.api

import com.nerallan.core.Config
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object AdApiClient {

    val getAdClient: AdApiInterface by lazy {
        val retrofit = Retrofit.Builder()
            .baseUrl(Config.BASE_API_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        retrofit.create(AdApiInterface::class.java)
    }
}