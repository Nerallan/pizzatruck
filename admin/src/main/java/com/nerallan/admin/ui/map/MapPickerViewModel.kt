package com.nerallan.admin.ui.map

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.maps.model.LatLng
import com.nerallan.admin.ResponseStatus
import com.nerallan.admin.api.MapApiClient
import com.nerallan.admin.datasource.MapDataSource
import com.nerallan.admin.datasource.remote.MapRemoteDataSource
import com.nerallan.admin.model.Coordinates
import com.nerallan.admin.repository.MapRepository
import com.nerallan.admin.repository.impl.MapRepositoryImpl
import timber.log.Timber

class MapPickerViewModel : ViewModel() {

    private var repository: MapRepository
    private var status: MutableLiveData<ResponseStatus> = MutableLiveData()

    companion object {
        private const val TAG = "MenuViewModel"
    }

    init {
        val mapClient = MapApiClient.getClient
        val remoteDataSource: MapDataSource = MapRemoteDataSource(mapClient)
        repository = MapRepositoryImpl(remoteDataSource)
    }

    fun pushCoordinates(latLng: LatLng) {
        status.value = ResponseStatus.Loading
        status = repository.insertCoordinates(latLng = Coordinates(lat = latLng.latitude, lng = latLng.longitude), status = status)
    }

    fun getStatus(): LiveData<ResponseStatus> = status

    override fun onCleared() {
        super.onCleared()
        Timber.d("onCleared")
    }
}