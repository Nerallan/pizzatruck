package com.nerallan.admin.ui.menu

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.nerallan.admin.ResponseStatus
import com.nerallan.admin.api.PizzaApiClient
import com.nerallan.admin.datasource.PizzaDataSource
import com.nerallan.admin.datasource.remote.PizzaRemoteDataSource
import com.nerallan.admin.repository.PizzaRepository
import com.nerallan.admin.repository.impl.PizzaRepositoryImpl
import com.nerallan.core.db.entity.Pizza
import timber.log.Timber

class MenuViewModel : ViewModel() {

    private var repository: PizzaRepository
    var deleteItemStatus: MutableLiveData<ResponseStatus> = MutableLiveData()
    var pizzaList: MutableLiveData<List<Pizza>> = MutableLiveData()

    companion object {
        private const val TAG = "MenuViewModel"
    }

    init {
        val pizzaClient = PizzaApiClient.getClient
        val remoteDataSource: PizzaDataSource = PizzaRemoteDataSource(pizzaClient)
        repository = PizzaRepositoryImpl(remoteDataSource)
    }

    fun getMenu() {
        pizzaList = repository.getMenuPizza(pizzaList)
    }

    fun deleteItem(id: Int) {
        if (pizzaList.value != null) {
            val documentId: String = pizzaList.value!!.get(id).detailsUri
            deleteItemStatus.value = ResponseStatus.Loading
            deleteItemStatus = repository.deletePizza(documentId, deleteItemStatus)
        }
    }

    override fun onCleared() {
        super.onCleared()
        Timber.d( "onCleared")
    }
}