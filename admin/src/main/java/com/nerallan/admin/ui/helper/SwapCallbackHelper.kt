package com.nerallan.admin.ui.helper

import com.nerallan.admin.adapter.AdListAdapter
import com.nerallan.admin.model.AdDTO
import com.nerallan.admin.ui.AdServerOrderComparator

class SwapCallbackHelper(val list: MutableList<AdDTO>, private val adapter: AdListAdapter) : OrderChangeCallback {

    override fun onItemMove(fromPosition: Int, toPosition: Int) {
        list[fromPosition].order = toPosition
        list[toPosition].order = fromPosition
        list.sortWith(AdServerOrderComparator)
        adapter.notifyItemMoved(fromPosition, toPosition)
    }

    override fun onItemDismiss(position: Int) {
        list.removeAt(position)
        adapter.notifyItemRemoved(position)
    }
}