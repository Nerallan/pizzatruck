package com.nerallan.admin.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainViewModel : ViewModel() {
    private val selectedFragmentId = MutableLiveData<FragmentId>()

    fun selected(fragmentId: FragmentId) {
        selectedFragmentId.value = fragmentId
    }

    fun getSelectedFragment(): LiveData<FragmentId> = selectedFragmentId
}