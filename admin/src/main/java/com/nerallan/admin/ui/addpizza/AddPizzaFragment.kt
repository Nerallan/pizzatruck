package com.nerallan.admin.ui.addpizza

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.textfield.TextInputLayout
import com.nerallan.admin.R
import com.nerallan.admin.ResponseStatus
import com.nerallan.core.Config
import timber.log.Timber

class AddPizzaFragment : Fragment() {

    private lateinit var titleEditLayout: TextInputLayout
    private lateinit var ingredientsEditLayout: TextInputLayout
    private lateinit var costEditLayout: TextInputLayout
    private lateinit var titleEditText: EditText
    private lateinit var ingredientsEditText: EditText
    private lateinit var costEditText: EditText
    private lateinit var imagePickButton: Button
    private lateinit var pushPizzaButton: Button
    private lateinit var pizzaImageView: ImageView
    private lateinit var progressBar: ProgressBar
    private lateinit var toolbar: Toolbar

    private lateinit var onAddPizzaStateChangeListener: OnAddPizzaStateChangeListener
    private lateinit var addPizzaViewModel: AddPizzaViewModel
    private var selectedImageUri: Uri? = null
    private var pickButtonClicked = false

    companion object {
        fun newInstance(): AddPizzaFragment {
            return AddPizzaFragment()
        }

        private const val GALLERY_REQUEST_CODE: Int = 1
    }

    interface OnAddPizzaStateChangeListener {
        fun changeVisibility()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Timber.d("onAttach")
        if (context is OnAddPizzaStateChangeListener) {
            onAddPizzaStateChangeListener = context
        } else {
            throw RuntimeException("$context must implement OnAddPizzaStateChangeListener")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View? = inflater.inflate(R.layout.fragment_add_pizza, container, false)
        Timber.d( "onCreateView")
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onAddPizzaStateChangeListener.changeVisibility()
        Timber.d("onViewCreated")
        initViews(view)

        setToolbar()

        toolbar.setNavigationOnClickListener {
            requireActivity().onBackPressed()
        }

        imagePickButton.setOnClickListener {
            pickButtonClicked = true
            pickFromGallery()
        }

        pushPizzaButton.setOnClickListener {
            if (isValidInputs()) {
                if (selectedImageUri != null) {
                    val imageUri = selectedImageUri
                    if (imageUri != null) {
                        pushToServer(
                            titleEditText.text.toString(),
                            ingredientsEditText.text.toString(),
                            costEditText.text.toString(),
                            imageUri
                        )
                    }
                } else {
                    Toast.makeText(requireContext(), R.string.no_pizza_image_error, Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    private fun setToolbar() {
        with(activity as AppCompatActivity) {
            setSupportActionBar(toolbar)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
            toolbar.title = requireActivity().getString(R.string.add_pizza_tollbar_title)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        addPizzaViewModel = ViewModelProviders.of(this).get(AddPizzaViewModel::class.java)

        observeViewModel(addPizzaViewModel)
    }

    private fun showPushProgress() {
        progressBar.visibility = View.VISIBLE
    }

    private fun hidePushProgress() {
        progressBar.visibility = View.GONE
    }

    private fun observeViewModel(addPizzaViewModel: AddPizzaViewModel) {
        addPizzaViewModel.getResponseStatus().observe(viewLifecycleOwner, Observer<ResponseStatus> { responseStatus ->
            when (responseStatus) {
                is ResponseStatus.Loading -> {
                    setButtonStates(false)
                    showPushProgress()
                }
                is ResponseStatus.Error -> {
                    setButtonStates(false)
                    hidePushProgress()
                    Toast.makeText(requireActivity(), responseStatus.errorMessage, Toast.LENGTH_SHORT).show()
                }
                is ResponseStatus.Success -> {
                    setButtonStates(true)
                    hidePushProgress()
                    clearEditFields()
                    removeEditTextFocus()
                    clearImageView()
                    Toast.makeText(
                        requireActivity(),
                        getString(R.string.successfully_added_message),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Timber.d("onDestroyView")
        onAddPizzaStateChangeListener.changeVisibility()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GALLERY_REQUEST_CODE) {
                selectedImageUri = data?.data
                Timber.d("selected image $selectedImageUri")
                pizzaImageView.setImageURI(selectedImageUri)
            }
        }
    }

    private fun initViews(view: View) {
        titleEditLayout = view.findViewById(R.id.til_pizza_title)
        ingredientsEditLayout = view.findViewById(R.id.til_pizza_ingredients)
        costEditLayout = view.findViewById(R.id.til_pizza_cost)
        titleEditText = view.findViewById(R.id.input_title_edit_text)
        ingredientsEditText = view.findViewById(R.id.input_ingredients_edit_text)
        costEditText = view.findViewById(R.id.input_cost_edit_text)
        imagePickButton = view.findViewById(R.id.pick_image_button)
        pushPizzaButton = view.findViewById(R.id.push_button)
        pizzaImageView = view.findViewById(R.id.picked_pizza_image_view)
        progressBar = view.findViewById(R.id.add_pizza_progress_bar)
        toolbar = view.findViewById(R.id.add_pizza_toolbar)
    }

    private fun pushToServer(title: String, ingredients: String, cost: String, selectedImageUri: Uri) {
        addPizzaViewModel.pushPizza(title, ingredients, cost, selectedImageUri)
    }

    private fun clearEditFields() {
        titleEditText.text.clear()
        ingredientsEditText.text.clear()
        costEditText.text.clear()
    }

    private fun removeEditTextFocus() {
        titleEditText.clearFocus()
        ingredientsEditText.clearFocus()
        costEditText.clearFocus()
    }

    private fun clearImageView() {
        if (isAdded) {
            pizzaImageView.setImageDrawable(
                ContextCompat.getDrawable(
                    requireContext(),
                    R.drawable.ic_pizza_placeholder
                )
            )
        }
    }

    private fun isValidInputs(): Boolean {
        var valid = true
        val title = titleEditText.text.toString()
        if (title.isEmpty()) {
            titleEditLayout.error = getText(R.string.title_input_error)
            valid = false
        } else {
            titleEditLayout.error = null
        }
        val ingredients = ingredientsEditText.text.toString()
        if (ingredients.isEmpty() || ingredients.length < 4 || ingredients.length > 100) {
            ingredientsEditLayout.error = getText(R.string.ingredients_input_error)
            valid = false
        } else {
            ingredientsEditLayout.error = null
        }
        val cost = costEditText.text.toString()
        if (cost.isEmpty()) {
            costEditLayout.error = getText(R.string.cost_input_error)
            valid = false
        } else {
            costEditLayout.error = null
        }
        if (pizzaImageView.drawable == null && pickButtonClicked) {
            Toast.makeText(requireContext(), R.string.no_pizza_image_error, Toast.LENGTH_LONG).show()
            valid = false
        }
        return valid
    }

    private fun setButtonStates(state: Boolean) {
        pushPizzaButton.isEnabled = state
        imagePickButton.isEnabled = state
    }

    private fun pickFromGallery() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = Config.INTENT_IMAGE_TYPE
        val mimeTypes = Config.IMAGE_MIME_TYPE
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
        startActivityForResult(intent, GALLERY_REQUEST_CODE)
    }
}