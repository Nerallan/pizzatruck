package com.nerallan.admin.ui.addpizza

import android.net.Uri
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.nerallan.admin.ResponseStatus
import com.nerallan.admin.api.PizzaApiClient
import com.nerallan.admin.datasource.PizzaDataSource
import com.nerallan.admin.datasource.remote.PizzaRemoteDataSource
import com.nerallan.admin.model.PizzaDTO
import com.nerallan.admin.repository.PizzaRepository
import com.nerallan.admin.repository.impl.PizzaRepositoryImpl
import timber.log.Timber

class AddPizzaViewModel : ViewModel() {

    private var repository: PizzaRepository
    private var responseStatus: MutableLiveData<ResponseStatus> = MutableLiveData()

    companion object {
        private const val TAG = "AddPizzaViewModel"
    }

    init {
        val pizzaClient = PizzaApiClient.getClient
        val remoteDataSource: PizzaDataSource = PizzaRemoteDataSource(pizzaClient)
        repository = PizzaRepositoryImpl(remoteDataSource)
    }

    fun pushPizza(title: String, ingredients: String, cost: String, selectedImageUri: Uri) {
        val pizza = PizzaDTO(title, ingredients, cost)
        responseStatus.value = ResponseStatus.Loading
        repository.insertPizza(pizza, selectedImageUri, responseStatus)
    }

    fun getResponseStatus(): LiveData<ResponseStatus> = responseStatus

    override fun onCleared() {
        super.onCleared()
        Timber.d( "onCleared")
    }
}