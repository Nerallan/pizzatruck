package com.nerallan.admin.ui.map

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.gms.tasks.Task
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.nerallan.admin.R
import com.nerallan.admin.ResponseStatus
import timber.log.Timber

class MapPickerFragment :
    Fragment(),
    OnMapReadyCallback,
    GoogleMap.OnMapClickListener {

    private lateinit var confirmFAB: FloatingActionButton
    private lateinit var toolbar: Toolbar
    private lateinit var mapProgressBar: ProgressBar

    private var googleMap: GoogleMap? = null
    private var truckMarker: Marker? = null
    private lateinit var mapViewModel: MapPickerViewModel

    private var locationPermissionGranted: Boolean = false

    companion object {
        fun newInstance(): MapPickerFragment {
            return MapPickerFragment()
        }

        private const val TAG: String = "MapPickerFragment"
        private const val DEFAULT_ZOOM: Float = 16f
        private const val LOCATION_PERMISSION_CODE = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Timber.d( "onCreate")
        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Timber.d("onCreateView")
        val view: View = inflater.inflate(R.layout.fragment_map_picker, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Timber.d("onViewCreated")
        initMap()
        initViews(view)
        setToolbar()
        confirmFAB.setOnClickListener {
            if (truckMarker?.position != null) {
                mapViewModel.pushCoordinates(truckMarker!!.position)
            } else {
                if (activity != null && isAdded) {
                    Toast.makeText(activity, getString(R.string.no_truck_coordinates_error), Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        Timber.d("onActivityCreated")
        mapViewModel = ViewModelProviders.of(this).get(MapPickerViewModel::class.java)
        observeViewModel(mapViewModel)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        Timber.d("onCreateOptionsMenu")
        inflater.inflate(R.menu.toolbar_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        Timber.d( "onOptionsItemSelected")
        return when (item.itemId) {
            R.id.action_geolocate -> {
                pickCurrentPlace()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        locationPermissionGranted = false
        when (requestCode) {
            LOCATION_PERMISSION_CODE -> {
                if (grantResults.isNotEmpty()
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                )
                    locationPermissionGranted = true
            }
        }
    }

    private fun getLocationPermission() {
        if (ContextCompat.checkSelfPermission(
                requireContext().applicationContext,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            == PackageManager.PERMISSION_GRANTED &&
            ContextCompat.checkSelfPermission(
                requireContext().applicationContext,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
            == PackageManager.PERMISSION_GRANTED
        ) {
            locationPermissionGranted = true
        } else {
            requestPermissions(
                arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ), LOCATION_PERMISSION_CODE
            )
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        this.googleMap = googleMap

        // Enable the zoom controls for the map
        googleMap.uiSettings.isZoomControlsEnabled = true
        // Prompt the user for permission.
        getLocationPermission()
        getDeviceLocation()

        googleMap.setOnMapClickListener(this)
        if (locationPermissionGranted) {
            if (ActivityCompat.checkSelfPermission(
                    requireActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(
                    requireActivity(),
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return
            }
            // enable my location layer and draws indicator of current location
            googleMap.isMyLocationEnabled = true
            googleMap.uiSettings.isMyLocationButtonEnabled = false
        }
    }

    private fun initMap() {
        val mapFragment: SupportMapFragment? = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
    }

    private fun initViews(view: View) {
        toolbar = view.findViewById(R.id.map_picker_toolbar)
        confirmFAB = view.findViewById(R.id.confirm_fab)
        mapProgressBar = view.findViewById(R.id.map_progress_bar)
    }

    private fun observeViewModel(mapViewModel: MapPickerViewModel) {
        mapViewModel.getStatus().observe(viewLifecycleOwner, Observer<ResponseStatus> { responseStatus ->
            when (responseStatus) {
                is ResponseStatus.Loading -> {
                    showMapProgress()
                }
                is ResponseStatus.Error -> {
                    hideMapProgress()
                    if (activity != null && isAdded) {
                        Toast.makeText(activity, responseStatus.errorMessage, Toast.LENGTH_SHORT).show()
                    }
                }
                is ResponseStatus.Success -> {
                    hideMapProgress()
                    if (activity != null && isAdded) {
                        Toast.makeText(activity, getString(R.string.success_operation), Toast.LENGTH_SHORT).show()
                    }
                }
            }
        })
    }

    private fun setToolbar() {
        with(activity as AppCompatActivity) {
            setSupportActionBar(toolbar)
            supportActionBar?.setDisplayShowTitleEnabled(false)
            toolbar.title = requireActivity().getString(R.string.map_picker_tollbar_title)
        }
    }

    @SuppressLint("MissingPermission")
    private fun getDeviceLocation() {
        try {
            if (locationPermissionGranted) {
                val fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(requireActivity())
                val locationResult: Task<Location> = fusedLocationProviderClient.lastLocation
                locationResult.addOnSuccessListener { location: Location? ->
                    if (location != null) {
                        val latitude = location.latitude
                        val longitude = location.longitude
                        updateMarkerPosition(LatLng(latitude, longitude))
                        googleMap?.animateCamera(
                            CameraUpdateFactory.newLatLngZoom(
                                LatLng(latitude, longitude),
                                DEFAULT_ZOOM
                            )
                        )
                    }
                }
            }
        } catch (exception: Exception) {
            Timber.d( "Exception: ${exception.message}")
        }
    }

    private fun pickCurrentPlace() {
        if (googleMap == null) {
            return
        }

        if (locationPermissionGranted) {
            getDeviceLocation()
        } else {
            Timber.d(getString(R.string.permissions_not_granted))
            getLocationPermission()
        }
    }

    private fun bitmapDescriptorFromVector(context: Context, vectorResId: Int): BitmapDescriptor? {
        val vectorDrawable: Drawable? = ContextCompat.getDrawable(context, vectorResId)
        vectorDrawable?.setBounds(0, 0, vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight)
        var bitmap: Bitmap? = null
        if (vectorDrawable != null) {
            bitmap = Bitmap.createBitmap(
                vectorDrawable.intrinsicWidth,
                vectorDrawable.intrinsicHeight,
                Bitmap.Config.ARGB_8888
            )
            val canvas = Canvas(bitmap)
            vectorDrawable.draw(canvas)
        }
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    override fun onMapClick(latLng: LatLng?) {
        updateMarkerPosition(latLng)
    }

    private fun updateMarkerPosition(latLng: LatLng?) {
        truckMarker?.remove()
        truckMarker = googleMap?.addMarker(makeMarkerOptions(latLng))
    }

    private fun makeMarkerOptions(latLng: LatLng?): MarkerOptions? {
        val markerOptions: MarkerOptions? = latLng?.let {
            MarkerOptions()
                .title(getString(R.string.pizza_truck_marker_title))
                .position(it)
                .snippet(latLng.toString())
                .icon(bitmapDescriptorFromVector(requireContext().applicationContext, R.drawable.ic_pizza_svgrepo_com))
        }
        return markerOptions
    }

    private fun showMapProgress() {
        mapProgressBar.visibility = View.VISIBLE
    }

    private fun hideMapProgress() {
        mapProgressBar.visibility = View.GONE
    }
}