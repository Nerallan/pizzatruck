package com.nerallan.admin.ui.ad

import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.nerallan.admin.ItemType
import com.nerallan.admin.R
import com.nerallan.admin.ResponseStatus
import com.nerallan.admin.adapter.AdListAdapter
import com.nerallan.admin.model.AdDTO
import com.nerallan.admin.ui.helper.SimpleItemTouchHelperCallback
import com.nerallan.admin.ui.helper.SwapCallbackHelper
import com.nerallan.core.Config

class EditAdFragment :
    Fragment(),
    AdListAdapter.OnDragStartListener {

    private lateinit var adRecyclerView: RecyclerView
    private lateinit var adProgressBar: ProgressBar
    private lateinit var adViewModel: AdViewModel

    private lateinit var itemTouchHelper: ItemTouchHelper
    private var selectedImageUri: Uri? = null

    companion object {
        fun newInstance(): EditAdFragment {
            return EditAdFragment()
        }

        private const val TAG: String = "EditAdFragment"
        private const val GALLERY_REQUEST_CODE: Int = 1
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_ad_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews(view)
        setupRecyclerView()
    }

    override fun onResume() {
        super.onResume()
        loadAdList()
    }

    override fun onPause() {
        super.onPause()
        updateAdList()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        adViewModel = ViewModelProviders.of(this).get(AdViewModel::class.java)
        observeViewModel(adViewModel)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GALLERY_REQUEST_CODE) {
                selectedImageUri = data?.data
                selectedImageUri?.let { showConfirmAlertDialog(it) }
            }
        }
    }

    private fun initViews(view: View) {
        adRecyclerView = view.findViewById(R.id.recycler_view_ad)
        adProgressBar = view.findViewById(R.id.progress_bar_ad)
    }

    private fun observeViewModel(adViewModel: AdViewModel) {
        adViewModel.getAdList().observe(viewLifecycleOwner, Observer<MutableList<AdDTO>> { adList ->
            setData(adList = adList)
        })
        adViewModel.getResponseStatus().observe(viewLifecycleOwner, Observer<ResponseStatus> { responseStatus ->
            when (responseStatus) {
                is ResponseStatus.Loading -> {
                    showAdProgress()
                }
                is ResponseStatus.Error -> {
                    hideAdProgress()
                    if (activity != null && isAdded) {
                        Toast.makeText(requireActivity(), responseStatus.errorMessage, Toast.LENGTH_SHORT).show()
                    }
                }
                is ResponseStatus.Success -> {
                    hideAdProgress()
                    if (activity != null && isAdded) {
                        Toast.makeText(activity, getString(R.string.success_message), Toast.LENGTH_SHORT).show()
                    }
                }
            }
        })
    }

    private fun setupRecyclerView() {
        val gridLayoutManager: GridLayoutManager = GridLayoutManager(requireContext(), 2)
        adRecyclerView.layoutManager = gridLayoutManager
    }

    private fun setTouchCallback(swapCallbackHelper: SwapCallbackHelper) {
        val callback = SimpleItemTouchHelperCallback(swapCallbackHelper)
        itemTouchHelper = ItemTouchHelper(callback)
        itemTouchHelper.attachToRecyclerView(adRecyclerView)
    }

    private fun updateAdList() {
        adViewModel.pushList()
    }

    private fun loadAdList() {
        adViewModel.receiveAdList()
    }

    private fun setData(adList: MutableList<AdDTO>) {
        val adapter = AdListAdapter(
            adList,
            clickListener = { position: Int, itemType: ItemType ->
                when (itemType) {
                    ItemType.Item -> {
                        showDeleteItemAlertDialog(position)
                        Toast.makeText(requireContext(), "setData $position, item", Toast.LENGTH_SHORT).show()
                    }
                    ItemType.PlusItem -> {
                        pickFromGallery()
                        Toast.makeText(requireContext(), "setData $position, plusItem ", Toast.LENGTH_SHORT).show()
                    }
                }

            },
            onDragStartListener = this
        )
        adRecyclerView.adapter = adapter

        val swapCallbackHelper: SwapCallbackHelper = SwapCallbackHelper(list = adList, adapter = adapter)
        setTouchCallback(swapCallbackHelper)
    }

    private fun pickFromGallery() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = Config.INTENT_IMAGE_TYPE
        val mimeTypes = Config.IMAGE_MIME_TYPE
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
        startActivityForResult(intent, GALLERY_REQUEST_CODE)
    }

    private fun makeConfirmAlertDialog(imageUri: Uri): AlertDialog? {
        val selectedImageView: ImageView = ImageView(requireContext())
        selectedImageView.setImageURI(imageUri)
        val alertDialogBuilder: AlertDialog.Builder = AlertDialog.Builder(requireContext())
            .setMessage(getString(R.string.push_image_question))
            .setPositiveButton(getString(R.string.ok_answer), object : DialogInterface.OnClickListener {
                override fun onClick(dialogIterface: DialogInterface?, p1: Int) {
                    Toast.makeText(requireContext(), getString(R.string.ok_answer), Toast.LENGTH_SHORT).show()
                    selectedImageUri?.let { adViewModel.pushAd(it) }
                    dialogIterface?.dismiss()
                }
            })
            .setNegativeButton(getString(R.string.cancel_answer), object : DialogInterface.OnClickListener {
                override fun onClick(dialogIterface: DialogInterface?, p1: Int) {
                    Toast.makeText(requireContext(), getString(R.string.cancel_answer), Toast.LENGTH_SHORT).show()
                    dialogIterface?.dismiss()
                }
            })
            .setView(selectedImageView)
        return alertDialogBuilder.create()
    }

    private fun showDeleteItemAlertDialog(position: Int) {
        makeDeleteItemAlertDialog(position)?.show()
    }

    private fun makeDeleteItemAlertDialog(position: Int): AlertDialog? {
        val alertDialogBuilder: AlertDialog.Builder = AlertDialog.Builder(requireContext())
            .setMessage(getString(R.string.delete_item_question))
            .setPositiveButton(getString(R.string.ok_answer), object : DialogInterface.OnClickListener {
                override fun onClick(dialogIterface: DialogInterface?, p1: Int) {
                    Toast.makeText(activity, getString(R.string.ok_answer), Toast.LENGTH_SHORT).show()
                    adViewModel.deleteItem(position)
                    dialogIterface?.dismiss()
                }
            })
            .setNegativeButton(getString(R.string.cancel_answer), object : DialogInterface.OnClickListener {
                override fun onClick(dialogIterface: DialogInterface?, p1: Int) {
                    Toast.makeText(activity, getString(R.string.cancel_answer), Toast.LENGTH_SHORT).show()
                    dialogIterface?.dismiss()
                }
            })
        return alertDialogBuilder.create()
    }

    private fun showConfirmAlertDialog(selectedImageUri: Uri) {
        makeConfirmAlertDialog(imageUri = selectedImageUri)?.show()
    }

    private fun showAdProgress() {
        adProgressBar.visibility = View.VISIBLE
    }

    private fun hideAdProgress() {
        adProgressBar.visibility = View.GONE
    }

    override fun onDragStarted(viewHolder: RecyclerView.ViewHolder) {
        itemTouchHelper.startDrag(viewHolder)
    }
}