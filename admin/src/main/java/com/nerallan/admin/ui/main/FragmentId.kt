package com.nerallan.admin.ui.main

import androidx.fragment.app.Fragment
import com.nerallan.admin.ui.ad.EditAdFragment
import com.nerallan.admin.ui.info.EditInfoFragment
import com.nerallan.admin.ui.map.MapPickerFragment
import com.nerallan.admin.ui.menu.MenuFragment

sealed class FragmentId(val id: Int, val fragment: Fragment) {
    object EditMenu : FragmentId(1, MenuFragment.newInstance())
    object EditAd : FragmentId(2, EditAdFragment.newInstance())
    object MapPicker : FragmentId(3, MapPickerFragment.newInstance())
    object EditInfo : FragmentId(4, EditInfoFragment.newInstance())
}