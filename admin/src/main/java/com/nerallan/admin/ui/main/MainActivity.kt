package com.nerallan.admin.ui.main

import android.content.Context
import android.os.Bundle
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.nerallan.admin.R
import com.nerallan.admin.ui.addpizza.AddPizzaFragment
import timber.log.Timber

class MainActivity :
    AppCompatActivity(),
    AddPizzaFragment.OnAddPizzaStateChangeListener {

    private lateinit var internetErrorLinearLayout: LinearLayout
    private lateinit var bottomNavigation: BottomNavigationView
    private lateinit var mainViewModel: MainViewModel

    companion object {
        private const val TAG: String = "MainActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        internetErrorLinearLayout = findViewById(R.id.error_linear_layout)
        bottomNavigation = findViewById(R.id.navigation_view)
        bottomNavigation.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)

        mainViewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        mainViewModel.getSelectedFragment().observe(this, Observer<FragmentId> { fragmentId: FragmentId ->
            addFragment(fragmentId.fragment)
        })

        if (savedInstanceState == null) {
            mainViewModel.selected(FragmentId.EditMenu)
        }
    }

    override fun onCreateView(name: String, context: Context, attrs: AttributeSet): View? {
        return super.onCreateView(name, context, attrs)
    }

    private fun addFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.commit()
    }

    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener {
        when (it.itemId) {
            R.id.navigation_edit_menu -> {
                mainViewModel.selected(FragmentId.EditMenu)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_edit_ad -> {
                mainViewModel.selected(FragmentId.EditAd)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_map_picker -> {
                mainViewModel.selected(FragmentId.MapPicker)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_edit_info -> {
                mainViewModel.selected(FragmentId.EditInfo)
                return@OnNavigationItemSelectedListener true
            }
        }
        return@OnNavigationItemSelectedListener false
    }

    override fun changeVisibility() {
        // when AddPizzaFragment opens - backStackEntryCount == 1
        if (supportFragmentManager.backStackEntryCount > 0) {
            hideBottomNavigationView()
        } else {
            showBottomNavigationView()
        }
        Timber.d("count ${supportFragmentManager.backStackEntryCount}")
    }

    private fun hideBottomNavigationView() {
        bottomNavigation.clearAnimation()
        bottomNavigation.visibility = View.GONE
    }

    private fun showBottomNavigationView() {
        bottomNavigation.clearAnimation()
        bottomNavigation.visibility = View.VISIBLE
    }
}
