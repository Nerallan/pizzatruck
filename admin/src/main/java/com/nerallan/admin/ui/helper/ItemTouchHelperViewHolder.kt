package com.nerallan.admin.ui.helper

interface ItemTouchHelperViewHolder {

    fun onItemSelected()

    fun onItemClear()
}