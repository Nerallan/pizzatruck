package com.nerallan.admin.ui

import com.nerallan.admin.model.AdDTO

object AdServerOrderComparator: Comparator<AdDTO> {
    override fun compare(a: AdDTO, b: AdDTO): Int {
        return a.order - b.order
    }
}