package com.nerallan.admin.ui.menu

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.nerallan.admin.R
import com.nerallan.admin.ResponseStatus
import com.nerallan.admin.ui.addpizza.AddPizzaFragment
import com.nerallan.admin.ui.helper.SwipeToDeleteCallback
import com.nerallan.core.BaseMenuFragment
import com.nerallan.core.adapter.PizzaListAdapter
import com.nerallan.core.db.entity.Pizza
import timber.log.Timber

class MenuFragment : BaseMenuFragment() {


    private var listener: OnClickListener? = null
    private lateinit var recyclerViewMenu: RecyclerView
    private lateinit var menuProgressBar: ProgressBar
    private lateinit var menuViewModel: MenuViewModel
    private lateinit var constraintLayout: ConstraintLayout
    private lateinit var addItemFAB: FloatingActionButton

    companion object {
        fun newInstance(): MenuFragment {
            return MenuFragment()
        }
        private const val ADD_PIZZA_FRAGMENT_TAG: String = "com.nerallan.admin.ui.AddPizzaFragment"
    }

    interface OnClickListener {
        fun onItemClick(position: Int)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        Timber.d("onActivityCreated")
        menuViewModel = ViewModelProviders.of(this).get(MenuViewModel::class.java)
        observeViewModel(menuViewModel)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View? = inflater.inflate(R.layout.fragment_pizza_list, container, false)
        Timber.d("onCreateView")
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Timber.d("onViewCreated")
        initViews(view)
        setupRecyclerView()

        addItemFAB.setOnClickListener {
            showAddPizzaFragment()
        }
        enableSwipe()
    }

    override fun onResume() {
        super.onResume()
        Timber.d("onResume")
        menuViewModel.getMenu()
    }

    override fun onDestroy() {
        super.onDestroy()
        Timber.d("onDestroy")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Timber.d("onDestroyView")
    }

    override fun getLayout(): Int = R.layout.fragment_pizza_list

    private fun initViews(view: View) {
        recyclerViewMenu = view.findViewById(R.id.recycler_view_menu)
        menuProgressBar = view.findViewById(R.id.progress_bar_menu)
        constraintLayout = view.findViewById(R.id.constraint_layout_menu)
        addItemFAB = view.findViewById(R.id.add_item_fab)
    }

    private fun observeViewModel(menuViewModel: MenuViewModel) {
        menuViewModel.pizzaList.observe(viewLifecycleOwner, Observer<List<Pizza>> {
            hidePizzaProgress()
            setData(menuList = it)
        })
        menuViewModel.deleteItemStatus.observe(requireActivity(), Observer<ResponseStatus> { responseStatus ->
            when (responseStatus) {
                is ResponseStatus.Loading -> showPizzaProgress()
                is ResponseStatus.Success -> {
                    hidePizzaProgress()
                    Toast.makeText(requireActivity(), getString(R.string.successfully_delete_message), Toast.LENGTH_SHORT).show()
                }
                is ResponseStatus.Error -> {
                    hidePizzaProgress()
                    Toast.makeText(requireActivity(), getString(R.string.error_message), Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun showAddPizzaFragment() {
        val addPizzaFragment: AddPizzaFragment = AddPizzaFragment.newInstance()
        requireActivity().supportFragmentManager.beginTransaction()
            .replace(R.id.container, addPizzaFragment, ADD_PIZZA_FRAGMENT_TAG)
            .addToBackStack(null)
            .commit()
    }

    private fun setupRecyclerView() {
        val linearLayoutManager = LinearLayoutManager(context)
        recyclerViewMenu.layoutManager = linearLayoutManager
    }

    private fun setData(menuList: List<Pizza>) {
        val adapter = PizzaListAdapter(menuList as ArrayList<Pizza>) { position: Int ->
            listener?.onItemClick(position)
        }
        recyclerViewMenu.adapter = adapter
    }

    private fun enableSwipe() {
        val swipeHandler = object : SwipeToDeleteCallback(requireContext()) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val adapter = recyclerViewMenu.adapter as PizzaListAdapter
                menuViewModel.deleteItem(viewHolder.adapterPosition)
                adapter.removeItem(viewHolder.adapterPosition)
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeHandler)
        itemTouchHelper.attachToRecyclerView(recyclerViewMenu)
    }

    private fun showPizzaProgress() {
        menuProgressBar.visibility = View.VISIBLE
    }

    private fun hidePizzaProgress() {
        menuProgressBar.visibility = View.GONE
    }
}