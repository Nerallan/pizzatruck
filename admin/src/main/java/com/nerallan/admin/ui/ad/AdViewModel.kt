package com.nerallan.admin.ui.ad

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.nerallan.admin.ResponseStatus
import com.nerallan.admin.api.AdApiClient
import com.nerallan.admin.datasource.AdDataSource
import com.nerallan.admin.datasource.remote.AdRemoteDataSource
import com.nerallan.admin.model.AdDTO
import com.nerallan.admin.repository.AdRepository
import com.nerallan.admin.repository.impl.AdRepositoryImpl
import com.nerallan.core.db.entity.Ad

class AdViewModel : ViewModel() {

    private var repository: AdRepository
    private var adList: MutableLiveData<MutableList<AdDTO>> = MutableLiveData()
    private var responseStatus: MutableLiveData<ResponseStatus> = MutableLiveData()

    init {
        val adClient = AdApiClient.getAdClient
        val remoteDataSource: AdDataSource = AdRemoteDataSource(adClient)
        repository = AdRepositoryImpl(remoteDataSource)
    }

    fun pushAd(selectedImageUri: Uri) {
        responseStatus.value = ResponseStatus.Loading
        var order = 0
        if (adList.value != null || adList.value!!.isNotEmpty()) {
            order = adList.value!!.size
        }
        repository.insertAd(AdDTO(imageUri = selectedImageUri.toString(), order = order), responseStatus) {
            receiveAdList()
        }
    }

    fun receiveAdList() {
        responseStatus.value = ResponseStatus.Loading
        adList = repository.getAdList(adList, responseStatus)
    }

    fun pushList(list: MutableLiveData<MutableList<AdDTO>> = adList) {
        responseStatus.value = ResponseStatus.Loading
        if (list.value != null) {
            responseStatus = repository.updateAdList(adList.value!!, responseStatus)
        }
    }

    fun deleteItem(deleteItemId: Int) {
        if (adList.value != null) {
            val documentId: String = adList.value!!.get(deleteItemId).imageId
            responseStatus.value = ResponseStatus.Loading
            responseStatus = repository.deleteAd(documentId, responseStatus) {
                receiveAdList()
            }
        }
        changeOrderRemainingItems(deleteItemId)
    }

    private fun changeOrderRemainingItems(deleteItemId: Int) {
        // TODO: not implemented yet
        if (adList.value != null) {
            val remainingItems: MutableList<Ad>
            (deleteItemId + 1 until adList.value!!.size).forEach { index: Int ->

            }
        }
    }

    fun getResponseStatus(): LiveData<ResponseStatus> = responseStatus

    fun getAdList(): LiveData<MutableList<AdDTO>> = adList
}