package com.nerallan.admin.ui.helper

interface OrderChangeCallback {

    fun onItemMove(fromPosition: Int, toPosition: Int)

    fun onItemDismiss(position: Int)
}