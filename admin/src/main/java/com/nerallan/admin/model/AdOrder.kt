package com.nerallan.admin.model

import com.google.gson.annotations.SerializedName

data class AdOrder(
    @SerializedName("stringValue") val orderString: String
)