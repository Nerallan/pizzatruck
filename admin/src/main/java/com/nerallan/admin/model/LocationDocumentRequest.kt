package com.nerallan.admin.model

import com.google.gson.annotations.SerializedName

data class LocationDocumentRequest(
    @SerializedName("fields") val locationFieldsRequest: LocationFieldsRequest
)