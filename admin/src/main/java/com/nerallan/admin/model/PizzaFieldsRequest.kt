package com.nerallan.admin.model

import com.google.gson.annotations.SerializedName

data class PizzaFieldsRequest(
    @SerializedName("name") val pizzaNameStringValue: PizzaName,
    @SerializedName("ingredients") val pizzaIngredients: PizzaIngredients,
    @SerializedName("cost") val pizzaCostStringValue: PizzaCost,
    @SerializedName("image") val pizzaImageStringValue: PizzaImage
)