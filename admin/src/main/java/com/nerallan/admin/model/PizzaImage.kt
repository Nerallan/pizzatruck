package com.nerallan.admin.model

import com.google.gson.annotations.SerializedName

data class PizzaImage(
    @SerializedName("stringValue") val imageStringValue: String
)