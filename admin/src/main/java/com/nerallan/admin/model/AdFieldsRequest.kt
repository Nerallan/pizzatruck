package com.nerallan.admin.model

import com.google.gson.annotations.SerializedName

data class AdFieldsRequest(
    @SerializedName("uri") val uri: AdUri,
    @SerializedName("order") val order: AdOrder
)