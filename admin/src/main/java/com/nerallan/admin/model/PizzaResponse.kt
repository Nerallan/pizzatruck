package com.nerallan.admin.model

data class PizzaResponse(
    val name: String,
    val ingredients: String,
    val cost: String,
    val image: String
)