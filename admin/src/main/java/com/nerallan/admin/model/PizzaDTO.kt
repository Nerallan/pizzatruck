package com.nerallan.admin.model

data class PizzaDTO(
    val title: String,
    val ingredients: String,
    val cost: String
)