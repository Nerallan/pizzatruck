package com.nerallan.admin.model

import com.google.gson.annotations.SerializedName

data class PizzaIngredients(
    @SerializedName("stringValue") val ingredientsValueServer: String
)