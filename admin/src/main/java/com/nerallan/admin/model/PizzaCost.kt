package com.nerallan.admin.model

import com.google.gson.annotations.SerializedName

data class PizzaCost(
    @SerializedName("stringValue") val costStringValue: String
)