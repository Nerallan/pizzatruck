package com.nerallan.admin.model

import com.google.gson.annotations.SerializedName

data class AdDocumentRequest(
    @SerializedName("fields") val adFieldsRequest: AdFieldsRequest
)