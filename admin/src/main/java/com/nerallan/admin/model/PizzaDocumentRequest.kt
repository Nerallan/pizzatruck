package com.nerallan.admin.model

import com.google.gson.annotations.SerializedName

data class PizzaDocumentRequest(
    @SerializedName("fields") val pizzaFieldsRequest: PizzaFieldsRequest
)