package com.nerallan.admin.model

data class Coordinates(
    var lat: Double,
    var lng: Double
)