package com.nerallan.admin.model

data class AdDTO(
    val imageUri: String,
    val imageId: String = "options field",
    var order: Int
)