package com.nerallan.admin.model

import com.google.gson.annotations.SerializedName

data class LngRequest(
    @SerializedName("stringValue") val lngString: String
)