package com.nerallan.admin.model

import com.google.gson.annotations.SerializedName

data class LatRequest(
    @SerializedName("stringValue") val latString: String
)