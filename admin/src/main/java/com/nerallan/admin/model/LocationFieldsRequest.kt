package com.nerallan.admin.model

import com.google.gson.annotations.SerializedName

data class LocationFieldsRequest(
    @SerializedName("lat") val lat: LatRequest,
    @SerializedName("lng") val lng: LngRequest
)