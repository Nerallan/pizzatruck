package com.nerallan.admin.model

import com.google.gson.annotations.SerializedName

data class PizzaName(
    @SerializedName("stringValue") val nameStringValue: String
)