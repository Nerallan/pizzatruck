package com.nerallan.admin.model

import com.google.gson.annotations.SerializedName

data class AdUri(
    @SerializedName("stringValue") val adUri: String
)