package com.nerallan.admin.repository.impl

import android.net.Uri
import android.util.Log
import androidx.core.net.toUri
import androidx.lifecycle.MutableLiveData
import com.nerallan.admin.ResponseStatus
import com.nerallan.admin.datasource.AdDataSource
import com.nerallan.admin.datasource.ImageLoader
import com.nerallan.admin.model.AdDTO
import com.nerallan.admin.repository.AdRepository
import timber.log.Timber

class AdRepositoryImpl(private val adRemoteDataSource: AdDataSource) : AdRepository {

    companion object {
        private const val TAG = "AdRepositoryImpl"
    }

    override fun insertAd(
        adDTO: AdDTO,
        responseStatus: MutableLiveData<ResponseStatus>,
        callback: () -> Unit
    ): MutableLiveData<ResponseStatus> {
        adRemoteDataSource.putImageToDataStore(adDTO.imageUri.toUri(), object : ImageLoader.LoadImageCallback {
            override fun onImageLoaded(result: String) {
                Timber.d("onImageLoaded $result")
                adRemoteDataSource.getImageUri(adDTO.imageUri.toUri(), object : ImageLoader.GetImageUriCallback {
                    override fun onUriReceived(uri: Uri) {
                        Timber.d("onUriReceived $uri")
                        adRemoteDataSource.pushAd(
                            AdDTO(imageUri = uri.toString(), order = adDTO.order),
                            responseStatus,
                            callback
                        )
                    }

                    override fun onErrorOccurred(error: String) {
                        responseStatus.value = ResponseStatus.Error(error)
                        Timber.d("onErrorOccurred $error")
                    }
                })
            }

            override fun onErrorOccurred(error: String) {
                responseStatus.value = ResponseStatus.Error(error)
                Timber.d( "onErrorOccurred $error")
            }
        })
        return responseStatus
    }

    override fun updateAdList(
        adList: MutableList<AdDTO>,
        responseStatus: MutableLiveData<ResponseStatus>
    ): MutableLiveData<ResponseStatus> {
        return adRemoteDataSource.updateAdList(adList, responseStatus)
    }

    override fun getAdList(
        adList: MutableLiveData<MutableList<AdDTO>>,
        responseStatus: MutableLiveData<ResponseStatus>
    ): MutableLiveData<MutableList<AdDTO>> {
        return adRemoteDataSource.getAdList(adList, responseStatus)
    }

    override fun deleteAd(
        documentId: String,
        responseStatus: MutableLiveData<ResponseStatus>,
        callback: () -> Unit
    ): MutableLiveData<ResponseStatus> {
        return adRemoteDataSource.deleteAd(documentId, responseStatus, callback)
    }
}