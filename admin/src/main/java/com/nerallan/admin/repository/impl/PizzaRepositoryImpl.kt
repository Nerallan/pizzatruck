package com.nerallan.admin.repository.impl

import android.net.Uri
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.nerallan.admin.ResponseStatus
import com.nerallan.admin.datasource.ImageLoader
import com.nerallan.admin.datasource.PizzaDataSource
import com.nerallan.admin.model.PizzaDTO
import com.nerallan.admin.model.PizzaResponse
import com.nerallan.admin.repository.PizzaRepository
import com.nerallan.core.db.entity.Pizza
import timber.log.Timber

class PizzaRepositoryImpl(private val pizzaRemoteDataSource: PizzaDataSource) : PizzaRepository {

    companion object {
        private const val TAG = "PizzaRepositoryImpl"
    }

    override fun insertPizza(
        pizza: PizzaDTO,
        selectedImageUri: Uri,
        status: MutableLiveData<ResponseStatus>
    ): MutableLiveData<ResponseStatus> {
        pizzaRemoteDataSource.putImageToDataStore(selectedImageUri, object : ImageLoader.LoadImageCallback {
            override fun onImageLoaded(result: String) {
                Timber.d( "onImageLoaded $result")
                pizzaRemoteDataSource.getImageUri(selectedImageUri, object : ImageLoader.GetImageUriCallback {
                    override fun onUriReceived(uri: Uri) {
                        Timber.d( "onUriReceived $uri")
                        val pizzaResponse: PizzaResponse =
                            PizzaResponse(pizza.title, pizza.ingredients, pizza.cost, uri.toString())
                        pizzaRemoteDataSource.addPizza(pizzaResponse, status)
                    }

                    override fun onErrorOccurred(error: String) {
                        status.value = ResponseStatus.Error(error)
                        Timber.d( "onErrorOccurred $error")
                    }
                })
            }

            override fun onErrorOccurred(error: String) {
                status.value = ResponseStatus.Error(error)
                Timber.d("onErrorOccurred $error")
            }
        })
        return status
    }

    override fun getMenuPizza(data: MutableLiveData<List<Pizza>>): MutableLiveData<List<Pizza>> {
        return pizzaRemoteDataSource.getPizzaList(data)
    }

    override fun deletePizza(
        documentId: String,
        deleteItemStatus: MutableLiveData<ResponseStatus>
    ): MutableLiveData<ResponseStatus> {
        return pizzaRemoteDataSource.deletePizza(documentId, deleteItemStatus)
    }
}