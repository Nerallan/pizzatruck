package com.nerallan.admin.repository

import android.net.Uri
import androidx.lifecycle.MutableLiveData
import com.nerallan.admin.ResponseStatus
import com.nerallan.admin.model.PizzaDTO
import com.nerallan.core.db.entity.Pizza

interface PizzaRepository {

    fun getMenuPizza(data: MutableLiveData<List<Pizza>>): MutableLiveData<List<Pizza>>

    fun insertPizza(
        pizza: PizzaDTO,
        selectedImageUri: Uri,
        status: MutableLiveData<ResponseStatus>
    ): MutableLiveData<ResponseStatus>

    fun deletePizza(
        documentId: String,
        deleteItemStatus: MutableLiveData<ResponseStatus>
    ): MutableLiveData<ResponseStatus>
}