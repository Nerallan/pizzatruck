package com.nerallan.admin.repository

import androidx.lifecycle.MutableLiveData
import com.nerallan.admin.ResponseStatus
import com.nerallan.admin.model.Coordinates

interface MapRepository {
    fun insertCoordinates(latLng: Coordinates, status: MutableLiveData<ResponseStatus>): MutableLiveData<ResponseStatus>
}