package com.nerallan.admin.repository.impl

import androidx.lifecycle.MutableLiveData
import com.nerallan.admin.ResponseStatus
import com.nerallan.admin.datasource.MapDataSource
import com.nerallan.admin.model.Coordinates
import com.nerallan.admin.repository.MapRepository

class MapRepositoryImpl(private val pizzaRemoteDataSource: MapDataSource) : MapRepository {

    override fun insertCoordinates(
        latLng: Coordinates,
        status: MutableLiveData<ResponseStatus>
    ): MutableLiveData<ResponseStatus> {
        return pizzaRemoteDataSource.addCoordinates(latLng, status)
    }
}