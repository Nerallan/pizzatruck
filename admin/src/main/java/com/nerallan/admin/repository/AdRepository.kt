package com.nerallan.admin.repository

import androidx.lifecycle.MutableLiveData
import com.nerallan.admin.ResponseStatus
import com.nerallan.admin.model.AdDTO

interface AdRepository {

    fun getAdList(
        adList: MutableLiveData<MutableList<AdDTO>>,
        responseStatus: MutableLiveData<ResponseStatus>
    ): MutableLiveData<MutableList<AdDTO>>

    fun insertAd(
        adDTO: AdDTO,
        responseStatus: MutableLiveData<ResponseStatus>,
        callback: () -> Unit
    ): MutableLiveData<ResponseStatus>

    fun updateAdList(
        adList: MutableList<AdDTO>,
        responseStatus: MutableLiveData<ResponseStatus>
    ): MutableLiveData<ResponseStatus>

    fun deleteAd(
        documentId: String,
        responseStatus: MutableLiveData<ResponseStatus>,
        callback: () -> Unit
    ): MutableLiveData<ResponseStatus>
}