package com.nerallan.admin.adapter

import android.view.View
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.nerallan.admin.R
import com.nerallan.admin.ui.helper.ItemTouchHelperViewHolder
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.add_ad_view.view.*

class AddItemViewHolder(override val containerView: View) :
    RecyclerView.ViewHolder(containerView),
    LayoutContainer,
    ItemTouchHelperViewHolder {

    private var addAdImageView: ImageView? = null

    companion object {
        private const val TAG: String = "AddItemViewHolder"
    }

    init {
        addAdImageView = itemView.add_ad_image
    }

    fun bind() {
        addAdImageView?.setImageDrawable(ContextCompat.getDrawable(itemView.context, R.drawable.ic_add_item))
    }

    override fun onItemSelected() {
        itemView.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.darkGrey))
    }

    override fun onItemClear() {
        itemView.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.White))
    }
}