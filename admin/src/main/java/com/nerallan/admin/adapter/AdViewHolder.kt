package com.nerallan.admin.adapter

import android.graphics.Color
import android.view.View
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.nerallan.admin.ui.helper.ItemTouchHelperViewHolder
import com.nerallan.core.R
import kotlinx.android.extensions.LayoutContainer

class AdViewHolder(override val containerView: View) :
    RecyclerView.ViewHolder(containerView),
    LayoutContainer,
    ItemTouchHelperViewHolder {

    private var adImage: ImageView? = null

    companion object {
        private const val TAG: String = "AdViewHolder"
    }

    init {
        adImage = itemView.findViewById(R.id.ad_image)
    }

    fun bind(imageUri: String): Unit = with(itemView) {
        adImage?.let {
            Glide
                .with(this)
                .load(imageUri)
                .placeholder(ContextCompat.getDrawable(itemView.context, R.drawable.ic_ad_placeholder))
                .diskCacheStrategy(DiskCacheStrategy.DATA)
                .into(it)
        }
    }

    override fun onItemSelected() {
        itemView.setBackgroundColor(Color.GRAY)
    }

    override fun onItemClear() {
        itemView.setBackgroundColor(Color.WHITE)
    }
}