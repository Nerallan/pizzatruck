package com.nerallan.admin.adapter

import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nerallan.admin.ItemType
import com.nerallan.admin.R
import com.nerallan.admin.model.AdDTO

class AdListAdapter(
    private val adapterAdList: MutableList<AdDTO>,
    private val clickListener: (Int, ItemType) -> Unit,
    private val onDragStartListener: OnDragStartListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var layoutInflater: LayoutInflater? = null

    companion object {
        private const val TAG: String = "AdListAdapter"
    }

    interface OnDragStartListener {
        fun onDragStarted(viewHolder: RecyclerView.ViewHolder)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.context)
        }
        when (viewType) {
            ItemType.Item.typeId -> {
                return AdViewHolder(
                    layoutInflater!!.inflate(
                        R.layout.ad_view,
                        parent,
                        false
                    )
                )
            }
            ItemType.PlusItem.typeId -> {
                return AddItemViewHolder(
                    layoutInflater!!.inflate(
                        R.layout.add_ad_view,
                        parent,
                        false
                    )
                )
            }
            else -> throw IllegalArgumentException("Invalid view type")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (position) {
            adapterAdList.size -> {
                val addItemViewHolder: AddItemViewHolder = holder as AddItemViewHolder
                addItemViewHolder.bind()
                addItemViewHolder.itemView.setOnClickListener {
                    clickListener(holder.adapterPosition, ItemType.PlusItem)
                }
            }
            else -> {
                val adViewHolder: AdViewHolder = holder as AdViewHolder
                adViewHolder.bind(adapterAdList[position].imageUri)
                adViewHolder.itemView.setOnTouchListener { view, motionEvent ->
                    when (motionEvent.action) {
                        MotionEvent.ACTION_DOWN -> {
                            onDragStartListener.onDragStarted(holder)
                        }
                    }
                    return@setOnTouchListener false
                }

                adViewHolder.itemView.setOnClickListener {
                    clickListener(holder.adapterPosition, ItemType.Item)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        // last element of list for different view type (PlusItem)
        return (adapterAdList.size + 1)
    }

    override fun getItemViewType(position: Int): Int {
        if (position == (adapterAdList.size)) {
            return ItemType.PlusItem.typeId
        }
        return ItemType.Item.typeId
    }
}