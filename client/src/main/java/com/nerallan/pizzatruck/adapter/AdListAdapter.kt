package com.nerallan.pizzatruck.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nerallan.core.R
import com.nerallan.core.db.entity.Ad

class AdListAdapter(private val adapterAdList: List<Ad>) : RecyclerView.Adapter<AdViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdViewHolder {
        return AdViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.ad_view,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: AdViewHolder, position: Int) {
        holder.bind(adapterAdList[position].imageUri)
    }

    override fun getItemCount(): Int {
        return adapterAdList.size
    }
}