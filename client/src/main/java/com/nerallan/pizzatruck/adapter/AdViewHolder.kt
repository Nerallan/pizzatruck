package com.nerallan.pizzatruck.adapter

import android.view.View
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.nerallan.pizzatruck.R

class AdViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private var adImage: ImageView? = null

    init {
        adImage = itemView.findViewById(R.id.ad_image)
    }

    fun bind(imageUri: String) = with(itemView) {
        adImage?.let {
            Glide
                .with(this)
                .load(imageUri)
                .placeholder(ContextCompat.getDrawable(itemView.context, R.drawable.ic_ad_placeholder))
                .diskCacheStrategy(DiskCacheStrategy.DATA)
                .into(it)
        }
    }
}