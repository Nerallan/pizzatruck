package com.nerallan.pizzatruck.util

import android.content.Context
import androidx.fragment.app.Fragment

interface PermissionsHandler {

    fun providePermissionsHandler(context: Context, fragment: Fragment, permissionCode: Int): Boolean

    fun checkHasPermission(context: Context, permission: String): Boolean

    fun requestPermission(fragment: Fragment, permissions: Array<String>, requestCode: Int)
}