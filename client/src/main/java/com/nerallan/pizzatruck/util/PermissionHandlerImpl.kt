package com.nerallan.pizzatruck.util

import android.content.Context
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment

class PermissionHandlerImpl : PermissionsHandler {

    private var locationPermissionGranted: Boolean = false

    companion object {
        var FINE_LOCATION = android.Manifest.permission.ACCESS_FINE_LOCATION
        var COASE_LOCATION = android.Manifest.permission.ACCESS_COARSE_LOCATION
    }

    override fun providePermissionsHandler(context: Context, fragment: Fragment, permissionCode: Int): Boolean {
        val permissions: Array<String> = arrayOf(COASE_LOCATION, FINE_LOCATION)
        if (checkHasPermission(context, FINE_LOCATION) && checkHasPermission(context, COASE_LOCATION)) {
            locationPermissionGranted = true
            // all permissions already granted
        } else {
            requestPermission(fragment, permissions, permissionCode)
        }
        return locationPermissionGranted
    }

    override fun checkHasPermission(context: Context, permission: String): Boolean {
        return ContextCompat
            .checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED
    }

    override fun requestPermission(fragment: Fragment, permissions: Array<String>, requestCode: Int) {
        fragment.requestPermissions(permissions, requestCode)
    }
}