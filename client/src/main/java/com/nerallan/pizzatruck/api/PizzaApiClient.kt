package com.nerallan.pizzatruck.api

import com.nerallan.core.Config
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object PizzaApiClient {

    val getClient: PizzaApiInterface
        get() {
            val retrofit = Retrofit.Builder()
                .baseUrl(Config.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
            return retrofit.create(PizzaApiInterface::class.java)
        }
}