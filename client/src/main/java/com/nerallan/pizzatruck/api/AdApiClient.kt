package com.nerallan.pizzatruck.api

import android.graphics.Bitmap
import com.nerallan.core.Config
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object AdApiClient {

    val getAdClient: AdApiInterface
        get() {
            val retrofit = Retrofit.Builder()
                .baseUrl(Config.BASE_API_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            return retrofit.create(AdApiInterface::class.java)
        }
}