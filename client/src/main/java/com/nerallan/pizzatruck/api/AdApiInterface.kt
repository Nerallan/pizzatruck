package com.nerallan.pizzatruck.api

import com.nerallan.core.model.AdDocumentsResponse
import io.reactivex.Single
import retrofit2.http.GET

interface AdApiInterface {
    @GET("ad_images")
    fun getAdList(): Single<AdDocumentsResponse>
}