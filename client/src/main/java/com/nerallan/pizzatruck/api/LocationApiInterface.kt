package com.nerallan.pizzatruck.api

import com.google.gson.JsonObject
import com.nerallan.pizzatruck.model.LocationResponseBody
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Query

interface LocationApiInterface {
    @GET("location")
    fun getLocation(): Flowable<LocationResponseBody>

    @GET("route")
    fun getMapQuestRouteDirection(
        @Query("key") apiKey: String,
        @Query("from") origin: String,
        @Query("to") destination: String,
        @Query("outFormat") outFormat: String = "json",
        @Query("routeType") type: String = "shortest"
    ): Flowable<JsonObject>
}