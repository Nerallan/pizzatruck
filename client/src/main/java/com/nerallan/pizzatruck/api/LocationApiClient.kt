package com.nerallan.pizzatruck.api

import com.nerallan.core.Config
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object LocationApiClient {

    val loadLocation: LocationApiInterface
        get() {
            val retrofit = Retrofit.Builder()
                .baseUrl(Config.BASE_API_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            return retrofit.create(LocationApiInterface::class.java)
        }

    val loadRouteDirection: LocationApiInterface
        get() {
            val retrofit = Retrofit.Builder()
                .baseUrl(Config.ROUTE_DIRECTION_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            return retrofit.create(LocationApiInterface::class.java)
        }
}