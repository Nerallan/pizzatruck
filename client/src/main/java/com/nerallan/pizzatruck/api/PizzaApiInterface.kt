package com.nerallan.pizzatruck.api

import com.nerallan.core.model.PizzaDocumentResponse
import com.nerallan.core.model.PizzaDocumentsResponse
import io.reactivex.Flowable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface PizzaApiInterface {
    @GET("menu")
    fun getMenus(): Single<PizzaDocumentsResponse>

    @GET("menu/{id}")
    fun getPizzaById(@Path("id") id: String): Flowable<PizzaDocumentResponse>
}