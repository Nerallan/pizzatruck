package com.nerallan.pizzatruck.datasource

import com.nerallan.core.db.entity.Pizza
import io.reactivex.Flowable
import io.reactivex.Single

interface PizzaDataSource {

    fun getPizzaList(): Single<List<Pizza>>

    fun insertAllPizza(pizzaList: List<Pizza>)

    fun getPizzaByUri(pizzaUri: String): Flowable<Pizza>

    fun insertPizzaDetail(pizza: Pizza)

    fun clearAll()
}