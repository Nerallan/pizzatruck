package com.nerallan.pizzatruck.datasource.remote

import com.nerallan.core.Config
import com.nerallan.core.db.entity.Pizza
import com.nerallan.core.model.PizzaDocumentResponse
import com.nerallan.core.model.PizzaFieldsResponse
import com.nerallan.pizzatruck.api.PizzaApiInterface
import com.nerallan.pizzatruck.datasource.PizzaDataSource
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class RemotePizzaDataSource(private val pizzaApi: PizzaApiInterface) :
    PizzaDataSource {

    companion object {
        private const val TAG = "RemotePizzaDataSource"
        private const val STRING_VALUE_KEY = "stringValue"
    }

    override fun clearAll() {
        throw Exception("Insert not allowed in remote data source")
    }

    override fun insertPizzaDetail(pizza: Pizza) {
        throw Exception("Insert not allowed in remote data source")
    }

    override fun insertAllPizza(pizzaList: List<Pizza>) {
        throw Exception("Insert not allowed in remote data source")
    }

    override fun getPizzaByUri(pizzaUri: String): Flowable<Pizza> {
        return pizzaApi.getPizzaById(pizzaUri)
            .subscribeOn(Schedulers.io())
            .map {
                val pizzaFieldsResponse: PizzaFieldsResponse = it.pizzaFieldsResponse
                val detailsId: String = it.docName.substringAfterLast(Config.URI_MENU_SUBSTRING)
                val imageString: String = pizzaFieldsResponse.imageObject.get(STRING_VALUE_KEY).asString
                val pizza = Pizza(
                    pizzaFieldsResponse.nameObject.get(STRING_VALUE_KEY).asString,
                    pizzaFieldsResponse.ingredientsObject.get(STRING_VALUE_KEY).asString,
                    pizzaFieldsResponse.costObject.get(STRING_VALUE_KEY).asString,
                    imageString,
                    detailsId
                )
                Timber.d("get details from api")
                return@map pizza
            }
    }

    override fun getPizzaList(): Single<List<Pizza>> {
        return pizzaApi.getMenus()
            .subscribeOn(Schedulers.io())
            .map { responseBody ->
                val pizzaDocumentResponses: List<PizzaDocumentResponse>? = responseBody.pizzaDocumentResponses
                val pizzaList = ArrayList<Pizza>()
                for (doc in pizzaDocumentResponses!!) {
                    val pizzaFieldsResponse: PizzaFieldsResponse = doc.pizzaFieldsResponse
                    val detailsId: String = doc.docName.substringAfterLast(Config.URI_MENU_SUBSTRING)
                    pizzaList.add(
                        Pizza(
                            pizzaFieldsResponse.nameObject.get(STRING_VALUE_KEY).asString,
                            pizzaFieldsResponse.ingredientsObject.get(STRING_VALUE_KEY).asString,
                            pizzaFieldsResponse.costObject.get(STRING_VALUE_KEY).asString,
                            pizzaFieldsResponse.imageObject.get(STRING_VALUE_KEY).asString,
                            detailsId
                        )
                    )
                }
                Timber.d("get pizza list from api")
                return@map pizzaList.toList()
            }
    }
}