package com.nerallan.pizzatruck.datasource.remote

import com.nerallan.core.db.entity.Ad
import com.nerallan.core.model.AdDocumentResponse
import com.nerallan.core.model.AdFieldsResponse
import com.nerallan.pizzatruck.api.AdApiInterface
import com.nerallan.pizzatruck.datasource.AdDataSource
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class RemoteAdDataSource(private val adApiInterface: AdApiInterface) :
    AdDataSource {

    companion object {
        private const val TAG = "RemoteAdDataSource"
        private const val STRING_VALUE_KEY = "stringValue"
    }

    override fun insertAllAdList(adList: List<Ad>) {
        throw Exception("Insert not allowed in remote data source")
    }

    override fun getAdList(): Single<List<Ad>> {
        return adApiInterface
            .getAdList()
            .subscribeOn(Schedulers.io())
            .map { responseBody ->
                val adDocumentResponseList: List<AdDocumentResponse> = responseBody.adDocumentResponse
                val adUriList = ArrayList<Ad>()
                for (doc in adDocumentResponseList) {
                    val adFieldsResponse: AdFieldsResponse = doc.adFieldsResponse
                    val uri: String = adFieldsResponse.uri.get(STRING_VALUE_KEY).asString
                    val order: Int = adFieldsResponse.order.get(STRING_VALUE_KEY).asInt
                    adUriList.add(Ad(uri, order))
                }
                return@map adUriList.toList()
            }
    }
}