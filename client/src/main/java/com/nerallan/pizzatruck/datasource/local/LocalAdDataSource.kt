package com.nerallan.pizzatruck.datasource.local

import com.nerallan.core.db.dao.AdDao
import com.nerallan.core.db.entity.Ad
import com.nerallan.pizzatruck.datasource.AdDataSource
import io.reactivex.Single

class LocalAdDataSource(private val adDao: AdDao) : AdDataSource {

    companion object {
        private const val TAG = "LocalAdDataSource"
    }

    override fun getAdList(): Single<List<Ad>> {
        return adDao.getAll()
    }

    override fun insertAllAdList(adList: List<Ad>) {
        return adDao.insertAll(adList)
    }
}