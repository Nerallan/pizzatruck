package com.nerallan.pizzatruck.datasource.local

import com.nerallan.core.db.dao.PizzaDao
import com.nerallan.core.db.entity.Pizza
import com.nerallan.pizzatruck.datasource.PizzaDataSource
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class LocalPizzaDataSource(private val pizzaDao: PizzaDao) : PizzaDataSource {

    companion object {
        private const val TAG = "LocalPizzaDataSource"
    }

    override fun insertAllPizza(pizzaList: List<Pizza>) {
        return pizzaDao.insertAll(pizzaList)
    }

    override fun getPizzaByUri(pizzaUri: String): Flowable<Pizza> {
        return pizzaDao
            .getByUri(pizzaUri)
            .toFlowable()
            .subscribeOn(Schedulers.io())
    }

    override fun insertPizzaDetail(pizza: Pizza) {
        return pizzaDao.insert(pizza)

    }

    override fun getPizzaList(): Single<List<Pizza>> {
        return pizzaDao.getAll()
    }

    override fun clearAll() {
        return pizzaDao.clearTable()
    }
}