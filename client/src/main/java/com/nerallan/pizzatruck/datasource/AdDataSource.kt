package com.nerallan.pizzatruck.datasource

import com.nerallan.core.db.entity.Ad
import io.reactivex.Single

interface AdDataSource {

    fun getAdList(): Single<List<Ad>>

    fun insertAllAdList(adList: List<Ad>)
}