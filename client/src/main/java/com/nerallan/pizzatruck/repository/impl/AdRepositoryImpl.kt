package com.nerallan.pizzatruck.repository.impl

import com.nerallan.core.db.entity.Ad
import com.nerallan.pizzatruck.datasource.AdDataSource
import com.nerallan.pizzatruck.repository.AdRepository
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class AdRepositoryImpl(private val adRemoteDataSource: AdDataSource, private val adLocalDataSource: AdDataSource) :
    AdRepository {

    private var cachedAdList: ArrayList<Ad> = ArrayList()
    private var cacheIsDirty = false

    companion object {
        private const val TAG: String = "AdRepositoryImpl"
    }

    override fun loadAdList(): Single<List<Ad>> {

        // TODO: login for retrieve cached list it is't not empty, will be used in the future
//        if (cachedAdList.isNotEmpty() && !cacheIsDirty) {
//            return Flowable
//                .fromIterable(cachedAdList)
//                .toList()
//                .toFlowable()
//        } else {
//            cachedAdList = ArrayList()
//        }

        val remoteAdList: Single<List<Ad>> = getAndSaveRemoteList()
        return remoteAdList
            .onErrorResumeNext { throwable ->
                if (throwable is Exception) {
                    return@onErrorResumeNext getAndCacheLocalList()
                }
                return@onErrorResumeNext Single.error(throwable)
            }

        // TODO: retrieve localList first, if it's empty - retrieve remoteList, will be used in the future
//        return if (cacheIsDirty) {
//            remoteAdList
//        } else {
//            val localAdList: Flowable<List<Ad>> =  getAndCacheLocalList()
//            Flowable
//                .concat(localAdList, remoteAdList)
//                .filter{ pizzaList ->
//                    pizzaList.isNotEmpty()
//                }
//                .firstOrError()
//                .toFlowable()
//        }
    }

    private fun getAndCacheLocalList(): Single<List<Ad>> {
        return adLocalDataSource
            .getAdList()
            .subscribeOn(Schedulers.io())
            .doOnSuccess { adList ->
                cachedAdList.addAll(adList)
            }
    }

    private fun getAndSaveRemoteList(): Single<List<Ad>> {
        return adRemoteDataSource
            .getAdList()
            .subscribeOn(Schedulers.io())
            .doOnSuccess { adList: List<Ad> ->
                adLocalDataSource.insertAllAdList(adList = adList)
                cachedAdList.addAll(adList)
                Timber.d("getAndSaveRemoteList $adList")
            }.doAfterSuccess {
                cacheIsDirty = false
            }
    }
}
