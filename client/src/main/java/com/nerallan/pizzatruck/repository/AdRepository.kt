package com.nerallan.pizzatruck.repository

import com.nerallan.core.db.entity.Ad
import io.reactivex.Single

interface AdRepository {
    fun loadAdList(): Single<List<Ad>>
}