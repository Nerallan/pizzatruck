package com.nerallan.pizzatruck.repository.impl

import com.nerallan.core.db.entity.Pizza
import com.nerallan.pizzatruck.datasource.PizzaDataSource
import com.nerallan.pizzatruck.repository.PizzaRepository
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class PizzaRepositoryImpl(
    private val pizzaRemoteDataSource: PizzaDataSource,
    private val pizzaLocalDataSource: PizzaDataSource
) :
    PizzaRepository {

    private var cachedPizzaList: ArrayList<Pizza> = ArrayList()
    private var cacheIsDirty = false

    companion object {
        private const val TAG: String = "PizzaRepositoryImpl"
    }

    override fun getMenuPizza(): Single<List<Pizza>> {
        val remotePizzaList: Single<List<Pizza>> = getAndSaveRemoteList()
        return remotePizzaList
            .onErrorResumeNext { throwable ->
                if (throwable is Exception) {
                    return@onErrorResumeNext getAndCacheLocalList()
                }
                return@onErrorResumeNext Single.error(throwable)
            }
    }

    private fun getAndCacheLocalList(): Single<List<Pizza>>? {
        return pizzaLocalDataSource
            .getPizzaList()
            .subscribeOn(Schedulers.io())
            .doOnSuccess { listPizza ->
                cachedPizzaList.addAll(listPizza)
            }
    }

    private fun getAndSaveRemoteList(): Single<List<Pizza>> {
        return pizzaRemoteDataSource
            .getPizzaList()
            .subscribeOn(Schedulers.io())
            .doOnSuccess { pizzaList: List<Pizza> ->
                pizzaLocalDataSource.clearAll()
                pizzaLocalDataSource.insertAllPizza(pizzaList = pizzaList)
                cachedPizzaList.addAll(pizzaList)
            }.doAfterSuccess {
                cacheIsDirty = false
            }
    }

    override fun getPizzaDetails(pizzaUri: String): Flowable<Pizza> {
        val cachedPizza: Pizza? = getPizzaWithUriFromCache(pizzaUri)

        if (cachedPizza != null) {
            return Flowable.just(cachedPizza)
        }

        // if no cached pizza - load from local/server source
        if (cachedPizzaList.isEmpty()) {
            cachedPizzaList = ArrayList()
        }

        val localPizza: Flowable<Pizza> = getPizzaWithUriFromLocal(pizzaUri)
        val remotePizza: Flowable<Pizza> = pizzaRemoteDataSource
            .getPizzaByUri(pizzaUri)
            .doOnNext { pizza ->
                pizzaLocalDataSource.insertPizzaDetail(pizza)
                cachedPizzaList.add(pizza)
            }

        return Flowable.concat(localPizza, remotePizza)
            .firstElement()
            .toFlowable()
    }

    private fun getPizzaWithUriFromCache(pizzaUri: String): Pizza? {
        var targetPizza: Pizza? = null
        if (cachedPizzaList.isNotEmpty()) {
            for (pizza in cachedPizzaList) {
                if (pizza.detailsUri == pizzaUri) {
                    targetPizza = pizza
                }
            }
        }
        return targetPizza
    }

    private fun getPizzaWithUriFromLocal(pizzaUri: String): Flowable<Pizza> {
        return pizzaLocalDataSource
            .getPizzaByUri(pizzaUri)
            .doOnNext { pizza ->
                cachedPizzaList.add(pizza)
            }
            .firstElement()
            .toFlowable()
    }
}
