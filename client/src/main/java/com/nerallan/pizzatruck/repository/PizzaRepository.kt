package com.nerallan.pizzatruck.repository

import com.nerallan.core.db.entity.Pizza
import io.reactivex.Flowable
import io.reactivex.Single

interface PizzaRepository {

    fun getMenuPizza(): Single<List<Pizza>>

    fun getPizzaDetails(pizzaUri: String): Flowable<Pizza>
}