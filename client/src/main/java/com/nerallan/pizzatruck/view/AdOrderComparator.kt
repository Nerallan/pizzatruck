package com.nerallan.pizzatruck.view

import com.nerallan.core.db.entity.Ad


 object AdOrderComparator: Comparator<Ad> {
    override fun compare(a: Ad, b: Ad): Int {
        return a.orderIndex - b.orderIndex
    }
}