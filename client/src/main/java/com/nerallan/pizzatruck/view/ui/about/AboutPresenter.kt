package com.nerallan.pizzatruck.view.ui.about

class AboutPresenter(private val aboutView: AboutContract.AboutView) : AboutContract.AboutPresenter {

    override fun phoneNumberClicked() {
        aboutView.openCallApp()
    }

    override fun instIconClicked(packageName: String) {
        aboutView.openInstagramApp(packageName)
    }
}