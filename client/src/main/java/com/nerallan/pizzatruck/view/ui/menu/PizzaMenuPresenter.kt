package com.nerallan.pizzatruck.view.ui.menu

import com.nerallan.core.db.PizzaDatabase
import com.nerallan.core.db.dao.AdDao
import com.nerallan.core.db.dao.PizzaDao
import com.nerallan.core.db.entity.Ad
import com.nerallan.core.db.entity.Pizza
import com.nerallan.pizzatruck.MainApplication
import com.nerallan.pizzatruck.api.AdApiClient
import com.nerallan.pizzatruck.api.PizzaApiClient
import com.nerallan.pizzatruck.datasource.AdDataSource
import com.nerallan.pizzatruck.datasource.PizzaDataSource
import com.nerallan.pizzatruck.datasource.local.LocalAdDataSource
import com.nerallan.pizzatruck.datasource.local.LocalPizzaDataSource
import com.nerallan.pizzatruck.datasource.remote.RemoteAdDataSource
import com.nerallan.pizzatruck.datasource.remote.RemotePizzaDataSource
import com.nerallan.pizzatruck.repository.AdRepository
import com.nerallan.pizzatruck.repository.PizzaRepository
import com.nerallan.pizzatruck.repository.impl.AdRepositoryImpl
import com.nerallan.pizzatruck.repository.impl.PizzaRepositoryImpl
import com.nerallan.pizzatruck.view.AdOrderComparator
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy

class PizzaMenuPresenter(private val viewMenu: PizzaMenuContract.MenuView?) : PizzaMenuContract.MenuPresenter {

    private lateinit var pizzaList: List<Pizza>
    private val pizzaRepository: PizzaRepository
    private val adRepository: AdRepository

    private val disposable = CompositeDisposable()

    init {
        val pizzaClient = PizzaApiClient.getClient
        val pizzaDao: PizzaDao = PizzaDatabase.getInstance(MainApplication.applicationContext()).pizzaDao()
        val pizzaRemoteDataSource: PizzaDataSource = RemotePizzaDataSource(pizzaClient)
        val pizzaLocalDataSource: PizzaDataSource = LocalPizzaDataSource(pizzaDao)
        pizzaRepository = PizzaRepositoryImpl(pizzaRemoteDataSource, pizzaLocalDataSource)

        val adClient = AdApiClient.getAdClient
        val adDao: AdDao = PizzaDatabase.getInstance(MainApplication.applicationContext()).adDao()
        val adRemoteDataSource: AdDataSource = RemoteAdDataSource(adClient)
        val adLocalDataSource: AdDataSource = LocalAdDataSource(adDao)
        adRepository = AdRepositoryImpl(adRemoteDataSource, adLocalDataSource)
    }

    override fun subscribe() {
        viewMenu?.setPresenter(this)
    }

    override fun unsubscribe() {
        disposable.clear()
    }

    override fun getAd() {
        viewMenu?.showAdProgress()
        disposable.add(adRepository.loadAdList().observeOn(AndroidSchedulers.mainThread()).subscribeBy(
            onSuccess = { list ->
                val sortedList = sortAdList(list)
                sortedList?.let { onAdResultSuccess(it) }
            },
            onError = { e -> onAdResultFailed(e.toString()) }
        ))
    }

    override fun getMenu() {
        viewMenu?.showPizzaProgress()
        disposable.add(pizzaRepository.getMenuPizza().observeOn(AndroidSchedulers.mainThread()).subscribeBy(
            onSuccess = { list ->
                onPizzaResultSuccess(list)
                this.pizzaList = list
            },
            onError = { e -> onPizzaResultFailed(e.toString()) }
        ))
    }

    private fun onAdResultSuccess(adUriList: List<Ad>) {
        viewMenu?.hideAdProgress()
        viewMenu?.showAd(adUriList)
    }

    private fun onAdResultFailed(strError: String) {
        viewMenu?.hideAdProgress()
        viewMenu?.showError(strError)
    }

    private fun onPizzaResultSuccess(menuList: List<Pizza>) {
        viewMenu?.hidePizzaProgress()
        viewMenu?.showMenu(menuList)
    }

    private fun onPizzaResultFailed(strError: String) {
        viewMenu?.hidePizzaProgress()
        viewMenu?.showError(strError)
    }

    override fun onItemClick(position: Int) {
        viewMenu?.navigateToDetail(position, pizzaList)
    }

    private fun sortAdList(list: List<Ad>?): List<Ad>? {
        return list?.sortedWith(AdOrderComparator)
    }

    override fun isWinterTime() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
