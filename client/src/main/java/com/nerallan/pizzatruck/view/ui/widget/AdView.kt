package com.nerallan.pizzatruck.view.ui.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.nerallan.core.db.entity.Ad
import com.nerallan.pizzatruck.R
import com.nerallan.pizzatruck.adapter.AdListAdapter
import kotlinx.android.synthetic.main.fragment_ad_list.view.*

class AdView : LinearLayout {

    constructor(context: Context) : super(context)
    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet)

    init {
        initViews()
        setupRecyclerView()
    }

    private fun initViews() {
        View.inflate(context, R.layout.fragment_ad_list, this)
    }

    private fun setupRecyclerView() {
        val linearLayoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        recycler_view_ad.layoutManager = linearLayoutManager
    }

    fun setData(adList: List<Ad>) {
        val adapter = AdListAdapter(adList)
        recycler_view_ad.adapter = adapter
    }
}