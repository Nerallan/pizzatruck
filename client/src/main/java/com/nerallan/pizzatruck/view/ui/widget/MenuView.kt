package com.nerallan.pizzatruck.view.ui.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nerallan.core.adapter.PizzaListAdapter
import com.nerallan.core.db.entity.Pizza
import com.nerallan.pizzatruck.R

class MenuView : FrameLayout {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    private var listener: OnClickListener? = null
    private lateinit var recyclerViewMenu: RecyclerView

    init {
        initViews()
        setupRecyclerView()
    }

    interface OnClickListener {
        fun onItemClick(position: Int)
    }

    private fun initViews() {
        View.inflate(context, R.layout.fragment_pizza_list, this)
        recyclerViewMenu = findViewById(R.id.recycler_view_menu)
    }

    private fun setupRecyclerView() {
        val linearLayoutManager = LinearLayoutManager(context)
        recyclerViewMenu.layoutManager = linearLayoutManager
    }

    fun setData(menuList: List<Pizza>) {
        val adapter = PizzaListAdapter(menuList as ArrayList<Pizza>) { position: Int ->
            listener?.onItemClick(position)
        }
        recyclerViewMenu.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    fun setOnClickListener(listener: OnClickListener) {
        this.listener = listener
    }
}
