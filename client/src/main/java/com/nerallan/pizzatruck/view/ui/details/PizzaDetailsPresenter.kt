package com.nerallan.pizzatruck.view.ui.details

import com.nerallan.core.db.PizzaDatabase
import com.nerallan.core.db.dao.PizzaDao
import com.nerallan.core.db.entity.Pizza
import com.nerallan.pizzatruck.MainApplication
import com.nerallan.pizzatruck.api.PizzaApiClient
import com.nerallan.pizzatruck.datasource.PizzaDataSource
import com.nerallan.pizzatruck.datasource.local.LocalPizzaDataSource
import com.nerallan.pizzatruck.datasource.remote.RemotePizzaDataSource
import com.nerallan.pizzatruck.repository.PizzaRepository
import com.nerallan.pizzatruck.repository.impl.PizzaRepositoryImpl
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy

class PizzaDetailsPresenter(private val detailsView: PizzaDetailsContract.PizzaDetailsView?) :
    PizzaDetailsContract.PizzaDetailsPresenter {

    private val pizzaRepository: PizzaRepository

    private val disposable = CompositeDisposable()

    init {
        val pizzaClient = PizzaApiClient.getClient
        val pizzaDao: PizzaDao = PizzaDatabase.getInstance(MainApplication.applicationContext()).pizzaDao()
        val pizzaRemoteDataSource: PizzaDataSource = RemotePizzaDataSource(pizzaClient)
        val pizzaLocalDataSource: PizzaDataSource = LocalPizzaDataSource(pizzaDao)
        pizzaRepository = PizzaRepositoryImpl(pizzaRemoteDataSource, pizzaLocalDataSource)
    }

    override fun subscribe() {
        detailsView?.setPresenter(this)
    }

    override fun unsubscribe() {
        disposable.clear()
    }

    override fun loadDetailPizza(pizzaId: String) {
        disposable.add(pizzaRepository.getPizzaDetails(pizzaId).observeOn(AndroidSchedulers.mainThread()).subscribeBy(
            onNext = { pizza ->
                onResultSuccess(pizza)
            },
            onError = { error ->
                onResultFailed(error.toString())
            }
        ))
    }

    private fun onResultSuccess(pizza: Pizza) {
        detailsView?.showPizzaDetails(pizza)
    }

    private fun onResultFailed(strError: String) {
        detailsView?.showError(strError)
    }

    override fun onDestroy() {
        disposable.dispose()
    }
}