package com.nerallan.pizzatruck.view.ui.details

import android.os.Build
import android.os.Bundle
import android.transition.TransitionInflater
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.google.android.material.appbar.AppBarLayout
import com.nerallan.core.db.entity.Pizza
import com.nerallan.pizzatruck.R
import kotlinx.android.synthetic.main.fragment_pizza_details.*

class PizzaDetailsFragment : Fragment(), PizzaDetailsContract.PizzaDetailsView {

    private lateinit var pizzaNameText: TextView
    private lateinit var pizzaIngredientsText: TextView
    private lateinit var pizzaCostText: TextView
    private lateinit var pizzaImageView: ImageView
    private lateinit var toolbar: Toolbar

    private lateinit var pizzaDetailPresenter: PizzaDetailsPresenter

    companion object {
        private const val TAG: String = "PizzaDetailsFragment"
        private const val EXTRA_TRANSITION_NAME: String = "transition_name"
        private const val PIZZA_SERVER_ID: String = "pizza_server_id"

        fun newInstance(pizzaId: String, transitionName: String?): PizzaDetailsFragment {
            val pizzaDetailsFragment = PizzaDetailsFragment()
            val bundle = Bundle()
            bundle.putString(EXTRA_TRANSITION_NAME, transitionName)
            bundle.putString(PIZZA_SERVER_ID, pizzaId)
            pizzaDetailsFragment.arguments = bundle
            return pizzaDetailsFragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        postponeEnterTransition()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Log.d(TAG, "onCreateView")
        return inflater.inflate(R.layout.fragment_pizza_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d(TAG, "onViewCreated")

        initViews(view)

        with(activity as AppCompatActivity) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back)
            setSupportActionBar(toolbar)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        val transitionName: String? = arguments?.getString(EXTRA_TRANSITION_NAME)
        val pizzaId: String? = arguments?.getString(PIZZA_SERVER_ID)
        pizzaDetailPresenter = PizzaDetailsPresenter(this)
        pizzaId?.let { pizzaDetailPresenter.loadDetailPizza(it) }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            pizzaImageView.transitionName = transitionName
        }

        toolbar.setNavigationOnClickListener {
            activity?.onBackPressed()
        }
    }

    override fun onResume() {
        super.onResume()
        expandToolbar()
    }

    override fun onDestroy() {
        super.onDestroy()
        pizzaDetailPresenter.unsubscribe()
    }

    override fun setPresenter(presenter: PizzaDetailsPresenter) {
        pizzaDetailPresenter = presenter
    }

    private fun expandToolbar() {
        app_bar_layout.setExpanded(true, true)
        app_bar_layout.isActivated = true
        val params = collapsing_toolbar_layout_details.getLayoutParams() as AppBarLayout.LayoutParams
        params.scrollFlags =
            AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL or AppBarLayout.LayoutParams.SCROLL_FLAG_EXIT_UNTIL_COLLAPSED
        collapsing_toolbar_layout_details.layoutParams = params
    }

    private fun initViews(view: View) {
        pizzaNameText = view.findViewById(R.id.pizza_name_text_view)
        pizzaIngredientsText = view.findViewById(R.id.pizza_ingredients_text_view)
        pizzaCostText = view.findViewById(R.id.pizza_cost_text_view)
        pizzaImageView = view.findViewById(R.id.pizza_detail_image_view)
        toolbar = view.findViewById(R.id.details_toolbar)
    }

    override fun showPizzaDetails(pizza: Pizza) {
        pizzaNameText.text = pizza.name
        pizzaIngredientsText.text = pizza.ingredients
        pizzaCostText.text = pizza.cost
        pizzaNameText.text = pizza.name

        Glide
            .with(this)
            .load(pizza.image)
            .skipMemoryCache(true)
            .into(pizzaImageView)
    }

    override fun showError(strError: String) {
        Log.d(TAG, "Some error happened!")
    }
}