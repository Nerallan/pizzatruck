package com.nerallan.pizzatruck.view.ui.about

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.nerallan.pizzatruck.R

class AboutFragment : Fragment(), AboutContract.AboutView {

    private lateinit var aboutPresenter: AboutPresenter
    private lateinit var phoneNumberTextView: TextView
    private lateinit var instIconImageView: ImageView

    companion object {
        fun newInstance(): AboutFragment = AboutFragment()
        private const val TAG: String = "AboutFragment"
        const val instPackageName: String = "com.instagram.android"
        const val instUri: String = "http://instagram.com/_u/pizzatruck.by"
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_about, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        aboutPresenter = AboutPresenter(this)
        initViews(view)
        phoneNumberTextView.setOnClickListener {
            aboutPresenter.phoneNumberClicked()
        }

        instIconImageView.setOnClickListener {
            aboutPresenter.instIconClicked(instPackageName)
        }
    }

    private fun initViews(view: View) {
        phoneNumberTextView = view.findViewById(R.id.call_number_text_view)
        instIconImageView = view.findViewById(R.id.instagram_image_view)
    }

    override fun openInstagramApp(packageName: String) {
        val uri: Uri = Uri.parse(instUri)
        val instIntent = Intent(Intent.ACTION_VIEW, uri)
        instIntent.setPackage(packageName)
        try {
            startActivity(instIntent)
        } catch (e: ActivityNotFoundException) {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(instUri)
                )
            )
        }
    }

    override fun openCallApp() {
        val callIntent = Intent(Intent.ACTION_DIAL)
        val phoneNumber: String = String.format("tel:%s", Uri.encode(resources.getString(R.string.call_number)))
        callIntent.data = Uri.parse(phoneNumber)
        startActivity(callIntent)
    }
}
