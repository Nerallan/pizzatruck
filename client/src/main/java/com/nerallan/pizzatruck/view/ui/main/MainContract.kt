package com.nerallan.pizzatruck.view.ui.main

interface MainContract {
    interface MainView {
        fun showMainFragment()
        fun showAboutFragment()
        fun showMapFragment()
    }

    interface MainPresenter {
        fun openMainFragment()
        fun openAboutFragment()
        fun openMapFragment()
    }
}