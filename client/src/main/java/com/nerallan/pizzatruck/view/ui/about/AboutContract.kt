package com.nerallan.pizzatruck.view.ui.about

interface AboutContract {

    interface AboutView {

        fun openInstagramApp(packageName: String)

        fun openCallApp()
    }

    interface AboutPresenter {

        fun phoneNumberClicked()

        fun instIconClicked(packageName: String)
    }
}