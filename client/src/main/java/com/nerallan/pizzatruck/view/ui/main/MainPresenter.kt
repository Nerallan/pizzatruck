package com.nerallan.pizzatruck.view.ui.main

class MainPresenter(private val view: MainContract.MainView) : MainContract.MainPresenter {

    override fun openMainFragment() {
        view.showMainFragment()
    }

    override fun openAboutFragment() {
        view.showAboutFragment()
    }

    override fun openMapFragment() {
        view.showMapFragment()
    }
}