package com.nerallan.pizzatruck.view

import android.view.View
import androidx.viewpager.widget.ViewPager
import com.nerallan.pizzatruck.R

class PageTransformer : ViewPager.PageTransformer {

    override fun transformPage(page: View, position: Float) {
        val pageWidth: Int = page.width
        val imageView: View = page.findViewById(R.id.pizza_detail_image_view)
        val newPosition: Float = -position * (pageWidth / 2)
        imageView.translationX = newPosition
    }
}