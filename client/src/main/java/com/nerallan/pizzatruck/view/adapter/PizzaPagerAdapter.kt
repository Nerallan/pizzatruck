package com.nerallan.pizzatruck.view.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.nerallan.core.db.entity.Pizza
import com.nerallan.pizzatruck.view.ui.details.PizzaDetailsFragment

class PizzaPagerAdapter(fragmentManager: FragmentManager, private val pizzaList: ArrayList<Pizza>) :
    FragmentStatePagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {
        val pizza: Pizza = pizzaList.get(position)
        return PizzaDetailsFragment.newInstance(pizza.detailsUri, pizza.name)
    }

    override fun getCount(): Int {
        return pizzaList.size
    }
}