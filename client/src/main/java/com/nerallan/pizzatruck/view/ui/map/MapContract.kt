package com.nerallan.pizzatruck.view.ui.map

import com.nerallan.pizzatruck.base.BasePresenter
import com.nerallan.pizzatruck.base.BaseView
import com.nerallan.pizzatruck.model.Location
import io.reactivex.Flowable

interface MapContract {
    interface MapView : BaseView<com.nerallan.pizzatruck.view.ui.map.MapPresenter> {

        fun initMap()

        fun moveCameraToPizzaTruck(latLng: Location, title: String, zoom: Float)

        fun moveCameraToRoute(coordinatesList: List<Location>)

        fun showRouteDirection(coordinatesList: List<Location>)

        fun addMarkerOptions(latLng: Location, title: String)

        fun showGoogleMaps(origin: String, destination: String)

        fun errorToast(strError: String)
    }

    interface MapPresenter : BasePresenter {

        fun getTruckLocationToShowOnMap()

        fun getTruckLocation()

        fun getRouteDirection(key: String)

        fun setDeviceLocation(latLng: Location)

        fun openGoogleMaps()
    }

    interface MapModel {

        fun loadCoordinates(): Flowable<Location>

        fun getRouteBetweenPoints(origin: String, destination: String, key: String): Flowable<ArrayList<Location>>
    }
}