package com.nerallan.pizzatruck.view.ui.menu

import com.nerallan.core.db.entity.Ad
import com.nerallan.core.db.entity.Pizza
import com.nerallan.pizzatruck.base.BasePresenter
import com.nerallan.pizzatruck.base.BaseView

interface PizzaMenuContract {

    interface MenuView : BaseView<PizzaMenuPresenter> {

        fun showPizzaProgress()

        fun hidePizzaProgress()

        fun showAdProgress()

        fun hideAdProgress()

        fun showMenu(menuList: List<Pizza>)

        fun showAd(adList: List<Ad>)

        fun showError(strError: String)

        fun navigateToDetail(position: Int, pizzaList: List<Pizza>)

        fun showWinterToolbarIcon()
    }

    interface MenuPresenter : BasePresenter {

        fun getMenu()

        fun getAd()

        fun onItemClick(position: Int)

        fun isWinterTime()
    }
}