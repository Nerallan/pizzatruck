package com.nerallan.pizzatruck.view

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.transition.TransitionInflater
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.nerallan.core.db.entity.Pizza
import com.nerallan.pizzatruck.R
import com.nerallan.pizzatruck.view.adapter.PizzaPagerAdapter
import timber.log.Timber

class PizzaViewPagerFragment : Fragment() {

    private lateinit var onDetailsStateChangeListener: OnDetailsStateChangeListener

    interface OnDetailsStateChangeListener {
        fun changeVisibility()
    }

    companion object {
        private const val TAG: String = "PizzaViewPagerFragment"
        private const val EXTRA_ITEM_POS = "item_pos"
        private const val EXTRA_PIZZA_ITEMS = "pizza_items"

        fun newInstance(currentItem: Int, pizzaList: ArrayList<Pizza>): PizzaViewPagerFragment {
            val pizzaViewpagerFragment = PizzaViewPagerFragment()
            val bundle = Bundle()
            bundle.putInt(EXTRA_ITEM_POS, currentItem)
            bundle.putParcelableArrayList(EXTRA_PIZZA_ITEMS, pizzaList)
            pizzaViewpagerFragment.arguments = bundle
            return pizzaViewpagerFragment
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Timber.d( "onAttach")
        if (context is OnDetailsStateChangeListener) {
            onDetailsStateChangeListener = context
        } else {
            throw RuntimeException("$context must implement OnDetailsStateChangeListener")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Timber.d( "onCreate")
        postponeEnterTransition()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        }
        sharedElementReturnTransition = null
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Timber.d("onCreateView")
        return inflater.inflate(R.layout.fragment_pizza_view_pager, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Timber.d( "onViewCreated")

        onDetailsStateChangeListener.changeVisibility()

        val currentItem: Int? = arguments?.getInt(EXTRA_ITEM_POS)
        val pizzaItems = arguments?.getParcelableArrayList<Pizza>(EXTRA_PIZZA_ITEMS)

        val animalPagerAdapter = pizzaItems?.let { PizzaPagerAdapter(childFragmentManager, it) }
        val viewPager = view.findViewById<View>(R.id.pizza_view_pager) as ViewPager
        if (currentItem != null) {
            viewPager.adapter = animalPagerAdapter
            viewPager.currentItem = currentItem
        }
        viewPager.setPageTransformer(false, PageTransformer())
        setupPageDivider(viewPager)
    }

    override fun onDestroy() {
        super.onDestroy()
        Timber.d("onDestroy")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Timber.d( "onDestroyView")
        onDetailsStateChangeListener.changeVisibility()
    }

    private fun setupPageDivider(viewPager: ViewPager) {
        val px = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2f, resources.displayMetrics))
        viewPager.pageMargin = px
        viewPager.setPageMarginDrawable(R.color.colorAccent)
    }
}