package com.nerallan.pizzatruck.view.ui.main

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.amitshekhar.DebugDB
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.nerallan.pizzatruck.R
import com.nerallan.pizzatruck.util.NetworkUtil
import com.nerallan.pizzatruck.view.PizzaViewPagerFragment
import com.nerallan.pizzatruck.view.ui.about.AboutFragment
import com.nerallan.pizzatruck.view.ui.map.MapFragment
import com.nerallan.pizzatruck.view.ui.menu.MenuFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy

class MainActivity : AppCompatActivity(),
                     MainContract.MainView,
                     PizzaViewPagerFragment.OnDetailsStateChangeListener {

    private lateinit var presenter: MainContract.MainPresenter
    private lateinit var internetErrorLinearLayout: LinearLayout
    private lateinit var bottomNavigation: BottomNavigationView

    companion object {
        private const val TAG: String = "MainActivity"
        private const val bottomVisibility: String = "bottomVisibility"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.d(TAG, "onCreate")

        presenter = MainPresenter(this)

        internetErrorLinearLayout = findViewById(R.id.error_linear_layout)
        bottomNavigation = findViewById(R.id.navigation_view)
        bottomNavigation.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)

        // open the first tab as default
        if (savedInstanceState == null) {
            presenter.openMainFragment()
        } else {
            savedInstanceState.get(bottomVisibility)
        }
        DebugDB.getAddressLog()
    }

    override fun onStart() {
        super.onStart()
        Log.d(TAG, "onStart")
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        Log.d(TAG, "onRestoreInstanceState")
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "onResume")
        isInternetAvailableAtStart()
        checkInternetConnection()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean(bottomVisibility, supportFragmentManager.backStackEntryCount > 0)
        Log.d(TAG, "onSaveInstanceState")
    }

    override fun onPause() {
        super.onPause()
        Log.d(TAG, "onPause")
    }

    override fun onStop() {
        super.onStop()
        Log.d(TAG, "onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy")
    }

    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener {
        when (it.itemId) {
            R.id.navigation_menu -> {
                presenter.openMainFragment()
                return@OnNavigationItemSelectedListener true
            }

            R.id.navigation_map -> {
                presenter.openMapFragment()
                return@OnNavigationItemSelectedListener true
            }

            R.id.navigation_about -> {
                presenter.openAboutFragment()
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun changeVisibility() {
        // when Details fragment opens - backStackEntryCount == 1
        if (supportFragmentManager.backStackEntryCount > 0) {
            Log.d(TAG, "count " + supportFragmentManager.backStackEntryCount)
            hideBottomNavigationView()
        } else {
            Log.d(TAG, "count " + supportFragmentManager.backStackEntryCount)
            showBottomNavigationView()
        }
    }

    private fun isInternetAvailableAtStart() {
        val isAvailable: Boolean = NetworkUtil.isNetworkAvailable(this)
        if (!isAvailable) {
            showInternetErrorView()
        }
    }

    @SuppressLint("CheckResult")
    private fun checkInternetConnection() {
        NetworkUtil.isInternetAvailabilityChange().observeOn(AndroidSchedulers.mainThread()).subscribeBy(
            onNext = { status ->
                if (status == true) {
                    hideInternetErrorView()
                } else {
                    showInternetErrorView()
                }
            },
            onError = { error -> Log.d(TAG, error.localizedMessage!!) }
        )
    }

    private fun showInternetErrorView() {
        internetErrorLinearLayout.visibility = View.VISIBLE
    }

    private fun hideInternetErrorView() {
        internetErrorLinearLayout.visibility = View.GONE
    }

    override fun showMainFragment() {
        val parentMenuFragment = MenuFragment.newInstance()
        addFragment(parentMenuFragment)
    }

    override fun showAboutFragment() {
        val infoFragment = AboutFragment.newInstance()
        addFragment(infoFragment)
    }

    override fun showMapFragment() {
        val fragmentMap = MapFragment.newInstance()
        addFragment(fragmentMap)
    }

    private fun addFragment(fragment: Fragment) {
        Handler().postDelayed({
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.container, fragment)
            transaction.commit()
        }, 100)
    }

    private fun hideBottomNavigationView() {
        bottomNavigation.clearAnimation()
        bottomNavigation.visibility = View.GONE
    }

    private fun showBottomNavigationView() {
        bottomNavigation.clearAnimation()
        bottomNavigation.visibility = View.VISIBLE
    }
}
