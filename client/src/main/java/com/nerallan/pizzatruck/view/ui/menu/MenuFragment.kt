package com.nerallan.pizzatruck.view.ui.menu

import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.ProgressBar
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.nerallan.core.BaseMenuFragment
import com.nerallan.core.db.entity.Ad
import com.nerallan.core.db.entity.Pizza
import com.nerallan.pizzatruck.R
import com.nerallan.pizzatruck.view.PizzaViewPagerFragment
import com.nerallan.pizzatruck.view.ui.widget.AdView
import com.nerallan.pizzatruck.view.ui.widget.MenuView
import java.text.SimpleDateFormat
import java.util.*

class MenuFragment : BaseMenuFragment(), PizzaMenuContract.MenuView {

    lateinit var menuPresenter: PizzaMenuPresenter
    private lateinit var menuView: MenuView
    private lateinit var adView: AdView

    private lateinit var menuProgressBar: ProgressBar
    private lateinit var adProgressBar: ProgressBar
    private lateinit var toolbar: Toolbar

    companion object {
        fun newInstance(): MenuFragment = MenuFragment()
        private const val TAG: String = "MenuFragment"
        private const val VIEW_PAGER_FRAGMENT_TAG = "com.nerallan.pizzatruck.PizzaViewPagerFragment"
        private const val START_WINTER_DATE: String = "12/01"
        private const val END_WINTER_DATE: String = "01/31"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)
        Log.d(TAG, "onCreate")
        menuPresenter = PizzaMenuPresenter(this)
        menuPresenter.subscribe()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d(TAG, "onViewCreated")

        if (isWinterTime()) {
            showWinterToolbarIcon()
        }

        initViews(view)
        menuView.setOnClickListener(object : MenuView.OnClickListener {
            override fun onItemClick(position: Int) {
                menuPresenter.onItemClick(position)
            }
        })
        menuPresenter.getMenu()
        menuPresenter.getAd()
    }

    override fun onStart() {
        super.onStart()
        Log.d(TAG, "onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "onResume")
    }

    override fun onStop() {
        super.onStop()
        Log.d(TAG, "onStop")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.d(TAG, "onDestroyView")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy")
        menuPresenter.unsubscribe()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.toolbar_menu, menu)
    }

    override fun getLayout(): Int = R.layout.fragment_parent_menu

    private fun initViews(view: View) {
        menuProgressBar = view.findViewById(R.id.progress_bar_menu)
        adProgressBar = view.findViewById(R.id.progress_bar_ad)
        menuView = view.findViewById(R.id.menu_view)
        adView = view.findViewById(R.id.ad_custom_view)
        toolbar = view.findViewById(R.id.toolbar_menu)
    }

    override fun setPresenter(presenter: PizzaMenuPresenter) {
        menuPresenter = presenter
    }

    override fun showMenu(menuList: List<Pizza>) {
        menuView.setData(menuList)
    }

    override fun showAd(adList: List<Ad>) {
        adView.setData(adList)
    }

    override fun showPizzaProgress() {
        menuProgressBar.visibility = View.VISIBLE
    }

    override fun hidePizzaProgress() {
        menuProgressBar.visibility = View.GONE
    }

    override fun showAdProgress() {
        adProgressBar.visibility = View.VISIBLE
    }

    override fun hideAdProgress() {
        adProgressBar.visibility = View.GONE
    }

    override fun showError(strError: String) {
        Log.d(TAG, strError)
    }

    override fun navigateToDetail(position: Int, pizzaList: List<Pizza>) {
        val pizzaViewPagerFragment: PizzaViewPagerFragment =
            PizzaViewPagerFragment.newInstance(position, pizzaList as ArrayList<Pizza>)
        fragmentManager?.let {
            it.beginTransaction()
                .addToBackStack(TAG)
                .replace(R.id.container, pizzaViewPagerFragment, VIEW_PAGER_FRAGMENT_TAG)
                .commit()
        }
    }

    private fun isWinterTime(): Boolean {
        val simpleDateFormat: SimpleDateFormat = SimpleDateFormat("MM/dd")
        val startDate: Date? = simpleDateFormat.parse(START_WINTER_DATE)
        val endDate: Date? = simpleDateFormat.parse(END_WINTER_DATE)
        return Date().after(startDate) && Date().before(endDate)
    }

    override fun showWinterToolbarIcon() {
    }
}
