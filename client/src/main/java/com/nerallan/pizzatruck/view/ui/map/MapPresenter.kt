package com.nerallan.pizzatruck.view.ui.map

import android.annotation.SuppressLint
import android.util.Log
import com.nerallan.pizzatruck.api.LocationApiClient
import com.nerallan.pizzatruck.model.Location
import com.nerallan.pizzatruck.model.LocationInteractor
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import java.util.*

class MapPresenter(private val mapView: MapContract.MapView) : MapContract.MapPresenter {

    private var locationMap: HashMap<String, Location> = HashMap<String, Location>()

    private val locationApiClient = LocationApiClient.loadLocation
    private val routeApiClient = LocationApiClient.loadRouteDirection
    private var locationApiDocument: LocationInteractor = LocationInteractor(locationApiClient)
    private var routeApiDocument: LocationInteractor = LocationInteractor(routeApiClient)
    private var cachedRouteList: ArrayList<Location> = ArrayList()

    companion object {
        private const val TAG = "MapPresenter"
        private const val DEFAULT_ZOOM = 16f
        const val MY_LOCATION_KEY = "My location"
        const val PIZZA_TRUCK_KEY = "Pizza Truck"
        const val PIZZA_TRUCK_MARKER_TITLE = "Pizza Truck"
    }

    override fun subscribe() {
        mapView.setPresenter(this)
    }

    override fun unsubscribe() {
    }

    override fun openGoogleMaps() {
        if (locationMap[MY_LOCATION_KEY] != null && locationMap[PIZZA_TRUCK_KEY] != null) {
            val params = makeUriParams(locationMap[MY_LOCATION_KEY]!!, locationMap[PIZZA_TRUCK_KEY]!!)
            mapView.showGoogleMaps(params[0], params[1])
        }
    }

    @SuppressLint("CheckResult")
    override fun getTruckLocationToShowOnMap() {
        locationApiDocument.loadCoordinates().observeOn(AndroidSchedulers.mainThread()).subscribeBy(
            onNext = { location ->
                locationMap[PIZZA_TRUCK_KEY] = location
                mapView.addMarkerOptions(location, PIZZA_TRUCK_MARKER_TITLE)
                onLocationResultSuccess(location)
            },
            onError = { error ->
                onLocationResultFailed("Impossible to get truck location")
                Log.d(TAG, "getTruckLocationToShowOnMap ${error.message}")
            }
        )
    }

    @SuppressLint("CheckResult")
    override fun getTruckLocation() {
        locationApiDocument.loadCoordinates().observeOn(AndroidSchedulers.mainThread()).subscribeBy(
            onNext = { location ->
                locationMap[PIZZA_TRUCK_KEY] = location
                mapView.addMarkerOptions(location, PIZZA_TRUCK_MARKER_TITLE)
            },
            onError = { error ->
                onLocationResultFailed("Impossible to get truck location")
                Log.d(TAG, "getTruckLocation ${error.message}")
            }
        )
    }

    @SuppressLint("CheckResult")
    override fun getRouteDirection(key: String) {
        if (locationMap[MY_LOCATION_KEY] != null && locationMap[PIZZA_TRUCK_KEY] != null) {
            val params = makeUriParams(locationMap[MY_LOCATION_KEY]!!, locationMap[PIZZA_TRUCK_KEY]!!)
            // load route direction only by first fab click
            if (cachedRouteList.isEmpty()) {
                routeApiDocument.getRouteBetweenPoints(params[0], params[1], key)
                    .observeOn(AndroidSchedulers.mainThread()).subscribeBy(
                    onNext = { coordinatesList ->
                        mapView.showRouteDirection(coordinatesList)
                        mapView.moveCameraToRoute(coordinatesList)
                    },
                    onError = { error ->
                        Log.d(TAG, error.message.toString())
                        Log.d(TAG, "getRouteDirection ${error.message}")
                    }
                )
            } else {
                mapView.moveCameraToRoute(cachedRouteList)
            }
        } else {
            onLocationResultFailed("Can't draw a route, try again")
        }
    }

    override fun setDeviceLocation(latLng: Location) {
        locationMap[MY_LOCATION_KEY] = latLng
    }

    private fun onLocationResultSuccess(location: Location) {
        mapView.moveCameraToPizzaTruck(location, "Pizza Truck", DEFAULT_ZOOM)
        Log.d(TAG, location.toString())
    }

    private fun onLocationResultFailed(strError: String) {
        mapView.errorToast(strError)
    }

    private fun makeUriParams(origin: Location, destination: Location): Array<String> {
        val originString = origin.lat.toString() + ',' + origin.lng.toString()
        val destinationString = destination.lat.toString() + ',' + destination.lng.toString()
        return arrayOf(originString, destinationString)
    }
}