package com.nerallan.pizzatruck.view.ui.map

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.nerallan.pizzatruck.MainApplication
import com.nerallan.pizzatruck.R
import com.nerallan.pizzatruck.util.PermissionHandlerImpl
import com.nerallan.pizzatruck.model.Location as LocationPOJO

class MapFragment : Fragment(), OnMapReadyCallback, MapContract.MapView {

    private var locationPermissionGranted: Boolean = false
    private var googleMap: GoogleMap? = null
    private lateinit var pizzaTruckFAB: FloatingActionButton
    private lateinit var directionFAB: FloatingActionButton
    private lateinit var googleMapFAB: FloatingActionButton
    private lateinit var mapPresenter: MapPresenter
    private var markerPizzaTruck: Marker? = null
    private var markerMyLocation: Marker? = null
    private var polylineRoute: Polyline? = null
    private lateinit var directionKey: String

    companion object {
        fun newInstance(): MapFragment = MapFragment()
        private const val TAG: String = "MapFragment"
        const val LOCATION_PERMISSION_CODE = 1
        const val PIZZA_TRUCK_MARKER_TITLE = "Pizza Truck"
        const val MY_LOCATION_MARKER_TITLE = "My location"
        private const val DEFAULT_ZOOM = 16f
        private const val POLYLINE_STROKE_WIDTH_PX = 10f
        private const val GAP_LENGTH_PX = 5f
        private val DOT: PatternItem = Dot()
        private val GAP: PatternItem = Gap(GAP_LENGTH_PX)
        private val PATTERN_POLYGON = listOf(GAP, DOT)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_map, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initMap()
        initViews(view)

        directionKey = resources.getString(R.string.direction_key)
        mapPresenter = MapPresenter(this)

        pizzaTruckFAB.setOnClickListener {
            mapPresenter.getTruckLocationToShowOnMap()
        }

        directionFAB.setOnClickListener {
            mapPresenter.getTruckLocation()
            if (locationPermissionGranted) {
                getDeviceLocation()
                mapPresenter.getRouteDirection(directionKey)
            } else {
                checkLocationPermission()
            }
        }

        googleMapFAB.setOnClickListener {
            mapPresenter.openGoogleMaps()
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        this.googleMap = googleMap

        checkLocationPermission()

        if (locationPermissionGranted) {
            if (ActivityCompat.checkSelfPermission(
                    requireActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(
                    requireActivity(),
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return
            }
            googleMap.isMyLocationEnabled = true
            googleMap.uiSettings.isMyLocationButtonEnabled = false
        }
        Log.d(TAG, "onMapReady: map is ready")
        Toast.makeText(activity, "Map is Ready", Toast.LENGTH_SHORT).show()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Log.d(TAG, "onRequestPermissionsResult: called")
        locationPermissionGranted = false
        // request permission called (application requested permission for the first time)
        when (requestCode) {
            LOCATION_PERMISSION_CODE -> {
                // if request canceled -> result arrays are empty
                if (grantResults.isNotEmpty()) {
                    for (grantResult in grantResults) {
                        // permission not granted
                        if (grantResult == PackageManager.PERMISSION_GRANTED) {
                            Log.d(TAG, "onRequestPermissionsResult: permission granted")
                            locationPermissionGranted = true
                            getDeviceLocation()
                        }
                    }
                }
            }
        }
    }

    private fun initViews(view: View) {
        pizzaTruckFAB = view.findViewById(R.id.pizza_truck_fab)
        directionFAB = view.findViewById(R.id.direction_fab)
        googleMapFAB = view.findViewById(R.id.google_map_fab)
    }

    override fun initMap() {
        val mapFragment: SupportMapFragment? = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
    }

    override fun setPresenter(presenter: MapPresenter) {
        mapPresenter = presenter
    }

    override fun showGoogleMaps(origin: String, destination: String) {
        val intent =
            Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?saddr=$origin&daddr=$destination"))
        startActivity(intent)
    }

    private fun checkLocationPermission() {
        val permissionsHandler = PermissionHandlerImpl()
        locationPermissionGranted =
            permissionsHandler.providePermissionsHandler(requireContext(), this, LOCATION_PERMISSION_CODE)
    }

    private fun getDeviceLocation() {
        Log.d(TAG, "getDeviceLocation: get the device current location")
        val fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(requireContext())
        try {
            if (locationPermissionGranted) {
                val fusedLocation = fusedLocationProviderClient.lastLocation
                fusedLocation.addOnSuccessListener { location: Location? ->
                    if (location != null) {
                        val latitude = location.latitude
                        val longitude = location.longitude

                        addMarkerOptions(
                            LocationPOJO(latitude, longitude),
                            MY_LOCATION_MARKER_TITLE
                        )
                        mapPresenter.setDeviceLocation(LocationPOJO(latitude, longitude))
                    }
                }
            }
        } catch (exception: SecurityException) {
            Log.d(TAG, "getDeviceLocation: SecurityException " + exception.message)
        }
    }

    override fun addMarkerOptions(latLng: LocationPOJO, title: String) {
        when (title) {
            MY_LOCATION_MARKER_TITLE -> {
                markerMyLocation?.remove()
                val markerOptions: MarkerOptions = MarkerOptions()
                    .position(LatLng(latLng.lat, latLng.lng))
                    .title(title)
                markerMyLocation = googleMap?.addMarker(markerOptions)
            }
            PIZZA_TRUCK_MARKER_TITLE -> {
                markerPizzaTruck?.remove()
                val markerOptions: MarkerOptions = MarkerOptions()
                    .position(LatLng(latLng.lat, latLng.lng))
                    .title(title)
                    .icon(
                        bitmapDescriptorFromVector(
                            MainApplication.applicationContext(),
                            R.drawable.ic_pizza_svgrepo_com
                        )
                    )
                markerPizzaTruck = googleMap?.addMarker(markerOptions)
            }
            else -> Log.d(TAG, getString(R.string.no_corresponding_title))
        }
    }

    override fun moveCameraToRoute(coordinatesList: List<LocationPOJO>) {
        zoomLocationsWithBounds(coordinatesList)
    }

    override fun moveCameraToPizzaTruck(latLng: LocationPOJO, title: String, zoom: Float) {
        Log.d(TAG, "moveCamera: moving camera to lat: " + latLng.lat + ", lng: " + latLng.lng)
        zoomLocation(latLng)
    }

    override fun showRouteDirection(coordinatesList: List<LocationPOJO>) {
        removePolylines()
        val polineOptions = PolylineOptions()
        for (coordinate in coordinatesList) {
            polineOptions.add(LatLng(coordinate.lat, coordinate.lng))
        }
        polineOptions
            .width(POLYLINE_STROKE_WIDTH_PX)
            .color(ContextCompat.getColor(requireContext(), R.color.colorPrimaryDark))
            .pattern(PATTERN_POLYGON)
        polylineRoute = googleMap?.addPolyline(polineOptions)
    }

    private fun removePolylines() {
        polylineRoute?.remove()
    }

    override fun errorToast(strError: String) {
        Toast.makeText(activity, strError, Toast.LENGTH_SHORT).show()
    }

    private fun bitmapDescriptorFromVector(context: Context, vectorResId: Int): BitmapDescriptor? {
        val vectorDrawable: Drawable? = ContextCompat.getDrawable(context, vectorResId)
        vectorDrawable?.setBounds(0, 0, vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight)
        var bitmap: Bitmap? = null
        if (vectorDrawable != null) {
            bitmap = Bitmap.createBitmap(
                vectorDrawable.intrinsicWidth,
                vectorDrawable.intrinsicHeight,
                Bitmap.Config.ARGB_8888
            )
            val canvas = Canvas(bitmap)
            vectorDrawable.draw(canvas)
        }
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    private fun zoomLocationsWithBounds(locations: List<LocationPOJO>) {
        val boundsBuilder: LatLngBounds.Builder = LatLngBounds.Builder()

        for (location in locations) {
            boundsBuilder.include(LatLng(location.lat, location.lng))
        }
        if (isAdded) {
            val width = (requireActivity().resources.displayMetrics.widthPixels * 0.7).toInt()
            val height = (requireActivity().resources.displayMetrics.heightPixels * 0.7).toInt()
            val routePadding = (width * 0.2).toInt()
            val latLngBounds = boundsBuilder.build()
            // TODO: use method in comments if not want to use animation for camera
            // googleMap?.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, width, height, routePadding))
            googleMap?.animateCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, width, height, routePadding))
        }
    }

    private fun zoomLocation(coordinate: LocationPOJO) {
        val location: CameraUpdate =
            CameraUpdateFactory.newLatLngZoom(LatLng(coordinate.lat, coordinate.lng), DEFAULT_ZOOM)
        googleMap?.animateCamera(location)
    }
}
