package com.nerallan.pizzatruck.view.ui.details

import com.nerallan.core.db.entity.Pizza
import com.nerallan.pizzatruck.base.BasePresenter
import com.nerallan.pizzatruck.base.BaseView

interface PizzaDetailsContract {
    interface PizzaDetailsView :
        BaseView<com.nerallan.pizzatruck.view.ui.details.PizzaDetailsPresenter> {

        fun showPizzaDetails(pizza: Pizza)

        fun showError(strError: String)
    }

    interface PizzaDetailsPresenter : BasePresenter {

        fun loadDetailPizza(pizzaId: String)

        fun onDestroy()
    }
}