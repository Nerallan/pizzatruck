package com.nerallan.pizzatruck.base

interface BasePresenter {

    fun subscribe()

    fun unsubscribe()
}
