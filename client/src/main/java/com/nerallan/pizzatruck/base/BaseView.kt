package com.nerallan.pizzatruck.base

interface BaseView<T> {
    fun setPresenter(presenter: T)
}