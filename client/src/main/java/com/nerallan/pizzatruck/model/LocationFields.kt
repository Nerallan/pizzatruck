package com.nerallan.pizzatruck.model

import com.google.gson.JsonObject
import com.google.gson.annotations.SerializedName

data class LocationFields(
    @SerializedName("lat")
    var latitude: JsonObject,
    @SerializedName("lng")
    var longitude: JsonObject
)