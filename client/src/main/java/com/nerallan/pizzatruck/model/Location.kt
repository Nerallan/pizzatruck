package com.nerallan.pizzatruck.model

data class Location(val lat: Double, val lng: Double)