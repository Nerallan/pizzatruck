package com.nerallan.pizzatruck.model

import com.google.gson.annotations.SerializedName

data class LocationDocument(
    @SerializedName("name")
    var docName: String,
    @SerializedName("fields")
    var locationFields: LocationFields,
    @SerializedName("createTime")
    var createTime: String,
    @SerializedName("updateTime")
    var updateTime: String
)