package com.nerallan.pizzatruck.model

import com.google.android.gms.maps.model.LatLng
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.nerallan.pizzatruck.api.LocationApiInterface
import com.nerallan.pizzatruck.view.ui.map.MapContract
import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class LocationInteractor(private val locationApi: LocationApiInterface) : MapContract.MapModel {

    companion object {
        private const val TAG: String = "LocationInteractor"
        private const val STRING_VALUE_KEY = "stringValue"
    }

    override fun loadCoordinates(): Flowable<Location> {
        return locationApi.getLocation()
            .subscribeOn(Schedulers.io())
            .map { responseBody ->
                val docResponse: List<LocationDocument>? = responseBody.locationDocument
                var latitude: Double? = null
                var longitude: Double? = null
                for (doc in docResponse!!) {
                    val locationFields: LocationFields? = doc.locationFields
                    if (locationFields != null) {
                        latitude = locationFields.latitude.get(STRING_VALUE_KEY).asDouble
                        longitude = locationFields.longitude.get(STRING_VALUE_KEY).asDouble
                        Timber.d( "onSuccess  ${LatLng(latitude, longitude)}")

                    }
                }
                return@map Location(latitude!!, longitude!!)
            }
    }

    override fun getRouteBetweenPoints(
        origin: String,
        destination: String,
        key: String
    ): Flowable<ArrayList<Location>> {
        return locationApi.getMapQuestRouteDirection(key, origin, destination)
            .subscribeOn(Schedulers.io())
            .map { responseBody ->
                val routeObject: JsonObject = responseBody.get("route") as JsonObject
                val legs: JsonArray = routeObject.get("legs") as JsonArray
                val pointList: ArrayList<Location> = ArrayList()
                for (leg in legs) {
                    val element: JsonObject = leg.asJsonObject
                    val maneuvers: JsonArray = element.get("maneuvers") as JsonArray
                    for (point in maneuvers) {
                        val pointObject: JsonObject = point.asJsonObject
                        val startPoint: JsonObject = pointObject.get("startPoint") as JsonObject

                        val lng: Double = startPoint.get("lng").asDouble
                        val lat: Double = startPoint.get("lat").asDouble
                        pointList.add(Location(lat, lng))
                    }
                }
                Timber.d("legs $legs")
                return@map pointList
            }
    }
}