package com.nerallan.pizzatruck.model

import com.google.gson.annotations.SerializedName

data class LocationResponseBody(
    @SerializedName("documents")
    var locationDocument: List<LocationDocument>
)