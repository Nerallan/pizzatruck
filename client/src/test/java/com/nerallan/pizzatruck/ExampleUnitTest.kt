package com.nerallan.pizzatruck

import com.nerallan.pizzatruck.view.ui.menu.PizzaMenuPresenter
import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        val pizzaMenuPresenter: PizzaMenuPresenter = PizzaMenuPresenter(null)
        assertEquals(4, pizzaMenuPresenter.getNumber())

    }
}
